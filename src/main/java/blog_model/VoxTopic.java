package blog_model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

// This class models the VoxEU topics.
@Data
@EqualsAndHashCode(callSuper = true)
public class VoxTopic extends AbstractTopicCategory {
    @NonNull
    private String topicURL;

    public VoxTopic(String topicURL, String topicLabel) {
        super(topicLabel);
        this.topicURL = topicURL;
    }
}