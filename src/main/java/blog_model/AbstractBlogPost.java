package blog_model;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * An abstract class to represent blog posts
 **/
@Data
public class AbstractBlogPost {
    private UUID uuid;
    private String URL;
    private String title;
    private List<String> authors;
    private LocalDate publicationDate;
    private String body;
}