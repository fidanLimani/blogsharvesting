package blog_model;

import lombok.Data;

/**
 * Blog posts contain different topic categorizations, at times even > 1 per blog (such as the case with VoxEU).
 * This class models the general aspects of these topics. Extending it for the individual blogs is expecte from every
 * harvested blog.
 */
@Data
public class AbstractTopicCategory {
    // Either "topic" or "term" label
    public String categoryLabel;

    public AbstractTopicCategory() {}

    public AbstractTopicCategory(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }
}