package blog_model;

import enums.HBSContentType;
import lombok.Data;

import java.time.LocalDate;

import java.util.List;

// This class models an HBS feed entry
@Data
public class FeedEntry {
    private String id;
    private String title;

    // The following 2 fields are used for updates (if such a scenario comes to play)
    private LocalDate datePublished;
    private LocalDate dateUpdated;
    // private LocalDateTime hbsDate; // This value can be derived from <datePublished> above.
    private String link; // link[@href]
    private String summary;

    private List<String> authors;

    // Examples include: "Working Knowledge".
    private List<String> sources;

    // This element contains HTML tags, too. Before using it for term assignment via Maui, remove them.
    private String content;

    private List<String> topics;
    private List<String> locations;
    private List<String> industries;

    // Examples include: "First look", "Working paper summaries, "Research & Ideas", etc.
    private HBSContentType contentType;

    // STW-based terms
    List<String> stwTerms;
}