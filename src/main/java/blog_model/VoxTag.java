package blog_model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

// VoxEU tags
@Data
@EqualsAndHashCode(callSuper = true)
public class VoxTag extends AbstractTopicCategory {
    @NonNull
    private String tagURL;

    public VoxTag(String tagURL, String tagLabel) {
        super(tagLabel);
        this.tagURL = tagURL;
    }
}