package blog_model;

import enums.VoxPostType;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

import java.util.List;
import java.util.UUID;

/**
 * While blog posts are similar, they all have their specifics. Extending the AbstractBogPost would allow us to capture
 * them.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VoxBlogPost extends AbstractBlogPost {
    // Is it a text or a podcast?
    private VoxPostType blogPostType;
    private String summary;
    private List<String> references;
    private List<VoxTopic> topics;
    private List<VoxTag> tags;
    private List<STWTerm> stwTerms;

    /**
     * Display the BlogPost attributes in a formatted way
     * @param blogPost A blog post instance
     **/
    public void displayBlogPostAttributes(VoxBlogPost blogPost) {
        System.out.println("UUDI: " + blogPost.getUuid());
        System.out.println("\nURL: " + blogPost.getURL());
        System.out.println("\nTitle: " + blogPost.getTitle());

        // Authors list: Check for null value
        if(blogPost.getAuthors() != null) {
            System.out.print("\nAuthors: ");
            for(String author : blogPost.getAuthors()) {
                System.out.print(author + "\t");
            }
        }

        System.out.println("\nPublication date: " + blogPost.getPublicationDate());
        System.out.println("\nBlog post type: " + blogPost.getBlogPostType());
        System.out.println("\nBlog post summary: " + blogPost.getSummary());

        // References list: Check for null value
        if(blogPost.getReferences() != null) {
            System.out.println("\nBlog post references: ");
            for(String reference : blogPost.getReferences()) {
                System.out.println(reference);
            }
        }

        // Post topics list: Check for null values
        if(blogPost.getTopics() != null) {
            System.out.println("\nBlog post topics: ");
            for(VoxTopic topic : blogPost.getTopics()) {
                System.out.println(topic.getCategoryLabel() + "\t" + topic.getTopicURL());
            }
        }

        // Post tags list: Check for null value
        if(blogPost.getTags() != null) {
            System.out.println("\nBlog post tags: ");
            for(VoxTag tag : blogPost.getTags()) {
                System.out.println(tag.getCategoryLabel() + "\t" + tag.getTagURL());
            }
        }
    }

    /**
     * Create an example blog post to experiment with.
     * @return aBlogPost An example blog post
     * */
    public static AbstractBlogPost generateTestBlogPost() {
        VoxBlogPost aBlogPost = new VoxBlogPost();
        aBlogPost.setUuid(UUID.fromString("b4ecf328-06b1-43d6-89fe-1fa0e4a3e22c"));
        aBlogPost.setURL("https://voxeu.org/article/googlefitbit-will-monetise-health-data-and-harm-consumers");
        aBlogPost.setTitle("Google/Fitbit will monetise health data and harm consumers");
        aBlogPost.setAuthors(List.of("Marc Bourreau", "Cristina Caffarra", "Gregory Crawford"));
        aBlogPost.setPublicationDate(LocalDate.parse("2020-09-30"));
        aBlogPost.setBlogPostType(VoxPostType.Text);
        aBlogPost.setSummary("The European Commission is conducting an in-depth investigation of the " +
                             "Google/Fitbit deal. A static, conventional view would suggest limited " +
                             "issues from a merger of complements. Yet, as this column outlines, " +
                             "unprecedented concerns arise when one sees that allowing for Fitbit’s data " +
                             "gathering capabilities to be put in Google’s hands creates major risks of " +
                             "“platform envelopment,” extension of monopoly power and consumer " +
                             "exploitation. The combination of Fitbit’s health data with Google’s other " +
                             "data creates unique opportunities for discrimination and exploitation of " +
                             "consumers in healthcare, health insurance and other sensitive areas, with " +
                             "major implications for privacy too. We also need to worry about incentives " +
                             "to pre-empt competition that could threaten Google’s data collection dominance. " +
                             "As the consensus is now firmly that preventing bad mergers is a key tool for " +
                             "competition policy vis-a-vis acquisitive digital platforms, the European " +
                             "Commission and other authorities should be very sceptical of this deal, " +
                             "and realistic about their limited ability to design, impose and monitor " +
                             "appropriate remedies.");
        aBlogPost.setBody("""
                          Google advertises its mission as “to organise the world’s information and make it universally
                          accessible and useful” – but its business practices of the last decade suggest a more accurate
                          statement: “to monetise the world’s data.”
                          Google’s “playbook” is by now well understood. Its history of systematic acquisitions across a
                          vast “chessboard” of disparate activities, bolted onto its original Search behemoth, are unified
                          by a common aim: to enhance and protect its unique data empire, and enable its monetisation in
                          ever-expanding applications. YouTube was acquired and, like Search, offered for free to consumers
                          to be monetised by selling “audiences” to advertisers. Android was acquired and then offered for
                          free to developers and handset manufacturers as part of a strategy to build the “bridge” that
                          enabled the shift of Google’s search monopoly from the desktop to mobile. Like Google’s suite
                          of popular “free” services (Gmail, Calendar, Maps, News, Chrome, etc.), it was not monetised
                          directly through the sale of ads, but through the collection of consumer demographic, interest,
                          and location data to better target the sale of ads. The acquisitions of Doubleclick and AdMob
                          sealed Google’s current monopoly in ad intermediation. In addition to these high-profile
                          acquisitions, Google has made dozens of others, mostly under the radar of competition agencies,
                          involving multiple complementary functionalities facilitating its mission of data collection,
                          data analysis, and exploitation. There is a term for this – “platform envelopment” (Eisenmann
                          et al. 2011): offering convenience to consumers while throttling competition. And (as the competition
                          authorities know well) the strategy has also involved a track record of proven anticompetitive
                          leveraging from one market into others (from Android to Shopping to adtech).
                          Monetisation of data has multiple aspects. As documented by the CMA, controlling access to vast
                          audiences allows Google to charge higher advertising prices;2 and controlling access to unparalleled
                          data about these audiences allows Google to carve them up according to demographics, interests
                          and locations, and to auction them to the highest bidder. And because it operates on all sides
                          of the ad tech stack, Google can infer advertisers’ willingness to pay and other publishers’
                          willingness to accept for audiences, and extract value from both. Unlike traditional monopolists,
                          Google’s dominance does not yield just “ordinary” market power: its essence is that of a
                          “discriminating” monopolist present across multiple markets, capable of harming consumers through
                          personalisation of advertising and increasingly also by enabling targeted product offerings,
                          accompanied by a record of leveraging its power both into adjacent markets and to protect its
                          existing dominance.
                          This discriminatory power, supported by Google’s unmatched data, is the dimension of the proposed
                          acquisition of Fitbit that concerns us most. The interest of Google in this deal is not in Fitbit’s
                          wearable itself – but only in the wearable as a source of valuable complementary health metrics
                          which Google can correlate with an enormous wealth of other data. Exploiting this data as a
                          general-purpose input, and flexing its discriminatory power, is something Google has done (very
                          profitably) for some time, and very openly, in online advertising markets. But such data has
                          many more possible applications and Google has the incentive, ability, and track record to leverage
                          its strength in data into many potential markets. There are known concerns around Google’s role
                          in facilitating financial scams, for example, that hurt the weak and vulnerable (Financial Times
                          2020a). Gathering health data that can be interacted with its existing data assets adds a whole
                          new level of concern around using a massive informational advantage to profitably exploit consumers
                          and put employees at risk. In particular, combining health and other data allows for personalisation
                          of offers in fields such as insurance, health, and even employment, that is incomparable and,
                          as we argue below, absolutely not benign or efficient. Looking beyond narrow consumer welfare,
                          an increased vulnerability of users as employees endangers the social fabric of Europe.
                          """);
        aBlogPost.setReferences(List.of("ACCC (2019), Digital Platforms Inquiry Final Report, 26 July.",
                                                        "Acemoglu, D, A Makhoumi, A Malekian and A Ozdaglar (2020), “Too much Data: Prices and inefficiencies in data markets”, NBER Working Paper No. 26296."));
        aBlogPost.setTopics(List.of(new VoxTopic("/content/topics/competition-policy", "Competition policy"),
                                                    new VoxTopic("/content/topics/eu-policies", "EU policies")));
        aBlogPost.setTags(List.of(
                new VoxTag("/taxonomy/term/2548", "Google"),
                new VoxTag("/taxonomy/term/10869",	"Fitbit"),
                new VoxTag("/taxonomy/term/11588", "health tech"),
                new VoxTag("/taxonomy/term/286", "European Commission"),
                new VoxTag("/taxonomy/term/11589", "data monetisation"),
                new VoxTag("/taxonomy/term/11590", "consumer exploitation")));

        aBlogPost.setStwTerms(List.of(
                new STWTerm("http://zbw.eu/stw/descriptor/10806-6", "Internationale Bank"),
                new STWTerm("http://zbw.eu/stw/descriptor/13776-2", "Bank lending")
        ));

        return aBlogPost;
    }
}