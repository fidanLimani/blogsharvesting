package blog_model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * STW Thesaurus terms: These are automatically inferred based on the blog post content.
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class STWTerm extends AbstractTopicCategory {
    @NonNull
    private String stwTermURL;

    public STWTerm(String stwTermURL, String stwTermLabel) {
        super(stwTermLabel);
        this.stwTermURL = stwTermURL;
    }
}