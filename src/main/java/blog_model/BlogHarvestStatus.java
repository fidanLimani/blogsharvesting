package blog_model;

import lombok.Data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

// This class contains harvest information that we store in a file to be used for later harvest updates.
@Data
public class BlogHarvestStatus {
    private String topic;
    private String topicURL;
    private int blogPostCount;
    private Set<String> postURLs;

    /*
    public static BlogHarvestStatus createTestInstance() {
        BlogHarvestStatus blogHarvestStatus = new BlogHarvestStatus();
        // Create few BlogHarvesterStatus instances
        blogHarvestStatus.setTopic("Competition policy");
        blogHarvestStatus.setBlogPostCount(10);
        blogHarvestStatus.setTopicURL("https://voxeu.org/content/topics/competition-policy");

        Set<String> postURLs = new HashSet<>(Arrays.asList("https://voxeu.org/vox-talks/patent-pools-generic-drugs",
                "https://voxeu.org/article/pooled-procurement-drugs-low-and-middle-income-countries-can-lower-prices-and-improve-access",
                "https://voxeu.org/article/balance-between-platform-variety-and-network-effects"));
        blogHarvestStatus.setPostURLs(postURLs);

        return blogHarvestStatus;
    }
     */
}