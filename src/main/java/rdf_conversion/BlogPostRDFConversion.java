package rdf_conversion;

import blog_model.*;

import blog_scrape.BlogScrapeVox;
import lombok.Data;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.time.Instant;

import java.util.List;

import static constants.RDFConversionConstants.*;

/** This class serializes a Blog post instance to RDF. */
@Data
public class BlogPostRDFConversion
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BlogPostRDFConversion.class);

    // Create a Model instance to contain the resulting blog post instance from the conversion to RDF
    private Model model;

    /** Default constructor */
    public BlogPostRDFConversion() {
        model = ModelFactory.createDefaultModel();
        setupPrefix();
        initializeLinkInstance();
    }

    // Classes and properties required for the RDF conversion
    Resource blogPostClass, blogPostInstance, blogPostCommentClass, linkProviderClass;

    Property hasKeywords, hasCitationCreationDate, linkProviderProperty, hasIdentifier, hasLiteralValue, usesIdentifierSchemeProperty,
            hasCitingEntityProperty, hasCitationCharacterizationProperty, hasCitationC, isCitedAsDataSourceByProperty,
            citesAsDataSourceProperty, citesAsRelatedProperty, hasCitedEntityProperty, citesForInformationProperty,
            isCitedForInformationByProperty, supplementProperty, supplementOfProperty, isRelatedToProperty, siocContent;

    // Setup the model namespace
    public void setupPrefix() {
        // LOGGER.info("Setting up namespace prefixes...");
        model.setNsPrefix(BLOG_POST_PREFIX, BLOG_POST_URI);
        model.setNsPrefix(IDENTIFIER_PREFIX, IDENTIFIER_URI);
        model.setNsPrefix(PROV_O_PREFIX, PROV_O_URI);
        model.setNsPrefix(AUTHOR_PREFIX, AUTHOR_URI);
        model.setNsPrefix(SIOC_PREFIX, SIOC_URI);
        model.setNsPrefix(SCHEMA_PREFIX, SCHEMA_URI);

        // Ontologies & Vocabularies used to represent Link attributes in RDF
        model.setNsPrefix(DC_TERMS_PREFIX, DC_TERMS);
        model.setNsPrefix(RDF_PREFIX, RDF_URI);
        model.setNsPrefix(RDFS_PREFIX, RDFS_URI);
        model.setNsPrefix(XSD_PREFIX, XSD);
        model.setNsPrefix(PROV_PREFIX, PROV);
    }

    // Initialize the RDF model and required namespaces, classes, and property instances.
    public void initializeLinkInstance() {
        blogPostClass = ResourceFactory.createResource(SIOC_URI + BLOG_POST);
        hasKeywords = ResourceFactory.createProperty(SCHEMA_URI, SCHEMA_KEYWORDS);
        siocContent = ResourceFactory.createProperty(SIOC_URI, SIOC_CONTENT);
    }

    /**
     * Convert Link instance: For every optional element, check whether it is present before attempting an operation on it.
     * @param blogPostResource A BlogPostResource instance
     */
    public Model convertBlogPost(VoxBlogPost blogPostResource) {
        // The model to store all blog post-related statements, and return it to the invoking party
        Model tempModel = ModelFactory.createDefaultModel();

        // Attributes: (0) BlogPost type, (1) URL identifier, and (2) title
        blogPostInstance = model.createResource(BLOG_POST_URI + blogPostResource.getUuid())
                .addProperty(RDF.type, blogPostClass)
                .addProperty(DCTerms.identifier, blogPostResource.getURL())
                .addProperty(DCTerms.title, blogPostResource.getTitle());

        // "Audio posts" do not have text as content!
        String blogPostBody = blogPostResource.getBody();
        if (blogPostBody != null && !blogPostBody.isEmpty()) {
          blogPostInstance.addProperty(siocContent, blogPostResource.getBody());
        }

        // (3) Blog post authors
        List<String> authors = blogPostResource.getAuthors();

        // Some blog post elements can be missing from a blog post
        if(authors != null && !authors.isEmpty()) {
            for(String author : authors) {
                String processedAuthor = author.replace(" ", "-").toLowerCase();

                // Link provider (1..N) Create the link provider instance
                Resource blogPostAuthor = model.createResource(AUTHOR_URI + processedAuthor)
                        .addProperty(RDFS.label, author);

                // Add each link provider to the link citation instance
                blogPostInstance.addProperty(DCTerms.creator, blogPostAuthor);
            }
        }

        // (4) Publication date: Some blog posts lack a publication date!
        if (blogPostResource.getPublicationDate() != null) {
            blogPostInstance.addProperty(DCTerms.created,
                    ResourceFactory.createTypedLiteral(blogPostResource.getPublicationDate().toString(),
                            XSDDatatype.XSDdate));
        }


        // (5) Blog post summary
        blogPostInstance.addProperty(DCTerms.abstract_, blogPostResource.getSummary());

        // (6) Blog post citations
        List<String> blogPostCitations = blogPostResource.getReferences();
        if (blogPostCitations != null && !blogPostCitations.isEmpty()) {
            Literal blogPostCitation;
            for(String citation : blogPostCitations) {
                blogPostCitation = ResourceFactory.createTypedLiteral(citation);
                blogPostInstance.addProperty(DCTerms.bibliographicCitation, blogPostCitation);
            }
        }

        // (7) Subject: Topics
        List<VoxTopic> blogPostTopics = blogPostResource.getTopics();
        Resource subjectResource;
        for (VoxTopic voxTopic : blogPostTopics) {
            String blogPostTopic = voxTopic.getTopicURL();
            // Generate the complete URL for the term; create a Resource, and add it a label
            // String subjectTermURL = String.format(VOX_BASE_URL, voxTopic.getCategoryLabel());
            subjectResource = model.createResource(blogPostTopic)
                                    .addProperty(RDFS.label, voxTopic.getCategoryLabel());
            blogPostInstance.addProperty(DCTerms.subject, subjectResource);
        }

        /* (8) Subject: STW terms: For "audio posts", there is no textual content, thus no means to infer STW terms
            based on it. Check it for null value! */
        List<STWTerm> blogPostSTWTerms = blogPostResource.getStwTerms();
        if (blogPostSTWTerms != null && !blogPostSTWTerms.isEmpty()) {
            Resource stwSubjectResource;
            for (STWTerm stwTerm : blogPostSTWTerms) {
                // Generate the complete URL for the term; create a Resource, and add it a label
                stwSubjectResource = model.createResource(stwTerm.getStwTermURL())
                        .addProperty(RDFS.label, stwTerm.getCategoryLabel());
                blogPostInstance.addProperty(DCTerms.subject, stwSubjectResource);
            }
        }

        // (8) Tags: Blog post "keywords"
        List<VoxTag> blogPostTags = blogPostResource.getTags();
        Resource blogPostTagResource;
        for (VoxTag aTag : blogPostTags) {
            // Generate the complete URL for the tag or keyword; create a Resource, and add it a label
            String blogPostTag = aTag.getTagURL();
            blogPostTagResource = model.createResource(blogPostTag)
                                        .addProperty(RDFS.label, aTag.getCategoryLabel());
            blogPostInstance.addProperty(hasKeywords, blogPostTagResource);
        }

          // (9) License information: Add it at the collection level, or at blog post instance level?
        blogPostInstance.addProperty(DCTerms.license, ResourceFactory
                                                        .createTypedLiteral(LICENSE_URL_BY_NC, XSDDatatype.XSDstring));

        // Store the RDF representation to a named graph (passed to the method)
        tempModel.add(model);
        // writeModelToFile("src/main/resources/blogPostInstance.ttl", model);

        // Ready the model for the next Link instance conversion
        model.removeAll();

        return tempModel;
    }

    /**
     * This method adds the provenance of the model, and it is invoked once the complete blog post collection
     * has been converted to RDF.
     * @param startedAt The time the conversion process starts at
     * @param endedAt The time the conversion process ends at (for the whole collection)
     * @return Model instance that contains the blog post collection provenance
     */
    public Model createProvenanceModel(Instant startedAt, Instant endedAt) {
        // A Model instance to store the provenance
        Model provModel = ModelFactory.createDefaultModel();
        Resource provCollectionClass, provCollectionInstance, provGenerationClass, provGenerationInstance,
                provActivityClass, provSoftwareAgentClass, provSoftwareAgentInstance,
                provActivityInstance;
        Property provGenerationActivityProperty, activityStartedAtProperty,
                activityEndedAtProperty, activityGeneratedProperty, provUsedProperty;

        // Initialize required classes, properties, and instances
        provCollectionClass = ResourceFactory.createResource(PROV + PROV_COLLECTION_CLASS);
        provGenerationClass = ResourceFactory.createResource(PROV + PROV_GENERATION_CLASS);
        provGenerationActivityProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_PROPERTY);
        provActivityClass = ResourceFactory.createResource(PROV + PROV_ACTIVITY_CLASS);
        provSoftwareAgentClass = ResourceFactory.createResource(PROV + PROV_SOFTWARE_AGENT_CLASS);
        activityStartedAtProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_START_TIME_VALUE);
        activityEndedAtProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_END_TIME_VALUE);
        activityGeneratedProperty = ResourceFactory.createProperty(PROV, PROV_GENERATED_VALUE);
        provUsedProperty = ResourceFactory.createProperty(PROV, PROV_USED_VALUE);

        // ProvGeneration instance
        provGenerationInstance = provModel.createResource(PROVENANCE_URI +
                                                          String.format(PROV_GENERATION_INSTANCE_VALUE,
                                                                        VOXEU_COLLECTION_TITLE));

        // Blog post in RDF representation
        provSoftwareAgentInstance = provModel.createResource(PROVENANCE_URI + PROV_ACTIVITY_INSTANCE)
            .addProperty(RDF.type, provSoftwareAgentClass);

        // The original blog post collection
        Property provWasGeneratedByProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_WAS_GENERATED_BY_VALUE);
        Property provQualifiedGenerationProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_QUALIFIED_GENERATION_VALUE);

        provCollectionInstance = provModel.createResource(PROVENANCE_URI + String.format(RDF_COLLECTION_INSTANCE,
                                                                            VOXEU_COLLECTION_TITLE))
                    .addProperty(RDF.type, provCollectionClass)
                    .addProperty(DCTerms.title, ResourceFactory.createTypedLiteral(VOXEU_COLLECTION_TITLE,
                            XSDDatatype.XSDstring))
                    .addProperty(DCTerms.description, ResourceFactory
                            .createTypedLiteral(ORIGINAL_COLLECTION_DESCRIPTION, XSDDatatype.XSDstring))
                    .addProperty(DCTerms.format, ResourceFactory.createTypedLiteral(ORIGINAL_COLLECTION_FORMAT,
                            XSDDatatype.XSDstring))
                    .addProperty(provWasGeneratedByProperty, provSoftwareAgentInstance)
                    .addProperty(provQualifiedGenerationProperty, provGenerationInstance);

        provActivityInstance = provModel.createResource(PROVENANCE_URI + String.format(BLOG_CONVERSION_VALUE, VOXEU_COLLECTION_TITLE))
            .addProperty(RDF.type, provActivityClass)
            .addProperty(activityStartedAtProperty, ResourceFactory.createTypedLiteral(startedAt.toString(), XSDDatatype.XSDstring))
            .addProperty(provUsedProperty, provCollectionInstance)
            .addProperty(activityGeneratedProperty, provCollectionInstance)
            .addProperty(activityEndedAtProperty, ResourceFactory.createTypedLiteral(endedAt.toString(), XSDDatatype.XSDstring));

        // Blog post collection in RDF representation
        provGenerationInstance
                .addProperty(RDF.type, provGenerationClass)
                .addProperty(RDFS.comment, PROV_GENERATION_COMMENT_VALUE)
                .addProperty(provGenerationActivityProperty, provActivityInstance);

        return provModel;
    }

    /**
     * This method adds the provenance of the model, and it is invoked once the complete blog post collection
     * has been converted to RDF.
     * @return Model instance that contains the blog post collection provenance
     */
    public Model createProvenanceModel() {
        // A Model instance to store the provenance
        Model provModel = ModelFactory.createDefaultModel();
        Resource provCollectionClass, provCollectionInstance, provGenerationClass, provGenerationInstance,
                provActivityClass, provSoftwareAgentClass, provSoftwareAgentInstance,
                provActivityInstance;
        Property provGenerationActivityProperty, activityStartedAtProperty,
                activityEndedAtProperty, activityGeneratedProperty, provUsedProperty;

        // Initialize required classes, properties, and instances
        provCollectionClass = ResourceFactory.createResource(PROV + PROV_COLLECTION_CLASS);
        provGenerationClass = ResourceFactory.createResource(PROV + PROV_GENERATION_CLASS);
        provGenerationActivityProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_PROPERTY);
        provActivityClass = ResourceFactory.createResource(PROV + PROV_ACTIVITY_CLASS);
        provSoftwareAgentClass = ResourceFactory.createResource(PROV + PROV_SOFTWARE_AGENT_CLASS);
        // activityStartedAtProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_START_TIME_VALUE);
        // activityEndedAtProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_END_TIME_VALUE);
        activityGeneratedProperty = ResourceFactory.createProperty(PROV, PROV_GENERATED_VALUE);
        provUsedProperty = ResourceFactory.createProperty(PROV, PROV_USED_VALUE);

        // ProvGeneration instance
        provGenerationInstance = provModel.createResource(PROVENANCE_URI +
                                                          String.format(PROV_GENERATION_INSTANCE_VALUE,
                                                                  VOXEU_COLLECTION_TITLE));

        // Blog post in RDF representation
        provSoftwareAgentInstance = provModel.createResource(PROVENANCE_URI + PROV_ACTIVITY_INSTANCE)
                .addProperty(RDF.type, provSoftwareAgentClass);

        // The original blog post collection
        Property provWasGeneratedByProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_WAS_GENERATED_BY_VALUE);
        Property provQualifiedGenerationProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_QUALIFIED_GENERATION_VALUE);

        provCollectionInstance = provModel.createResource(PROVENANCE_URI + String.format(RDF_COLLECTION_INSTANCE,
                VOXEU_COLLECTION_TITLE))
                .addProperty(RDF.type, provCollectionClass)
                .addProperty(DCTerms.title, ResourceFactory.createTypedLiteral(VOXEU_COLLECTION_TITLE,
                        XSDDatatype.XSDstring))
                .addProperty(DCTerms.description, ResourceFactory
                        .createTypedLiteral(ORIGINAL_COLLECTION_DESCRIPTION, XSDDatatype.XSDstring))
                .addProperty(DCTerms.format, ResourceFactory.createTypedLiteral(ORIGINAL_COLLECTION_FORMAT,
                        XSDDatatype.XSDstring))
                .addProperty(provWasGeneratedByProperty, provSoftwareAgentInstance)
                .addProperty(provQualifiedGenerationProperty, provGenerationInstance);

        provActivityInstance = provModel.createResource(PROVENANCE_URI + String.format(BLOG_CONVERSION_VALUE,
                                                                                            VOXEU_COLLECTION_TITLE))
                .addProperty(RDF.type, provActivityClass)
                .addProperty(provUsedProperty, provCollectionInstance)
                .addProperty(activityGeneratedProperty, provCollectionInstance);

        // Blog post collection in RDF representation
        provGenerationInstance
                .addProperty(RDF.type, provGenerationClass)
                .addProperty(RDFS.comment, PROV_GENERATION_COMMENT_VALUE)
                .addProperty(provGenerationActivityProperty, provActivityInstance);

        return provModel;
    }

    /**
     * Write the RDF model to a file. This is to test the conversion process with individual Link instances.
     * @param filePath The file to store the model contents to
     */
    private void writeModelToFile (String filePath, Model model) {
        // LOGGER.info("Writing RDF model to a store...");

        FileOutputStream fout;
        try {
            fout = new FileOutputStream(filePath);
            model.write(fout, "TURTLE");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Test the app
    public static void main(String[] args) {
        AbstractBlogPost testBlogPost = VoxBlogPost.generateTestBlogPost();

        // Test blog post properties
        System.out.println(testBlogPost);

        /*
        // An instance to create blog posts: A test case for the RDF conversion
        BlogScrapeVox blogScrapeVox = new BlogScrapeVox();
        String testURL1 = "https://voxeu.org/article/working-conditions-digital-labour-platforms";
        String testURL2 = "https://voxeu.org/article/spanning-hypothesis-and-risk-premia-long-term-bonds";

        // Conversion process starts (used for dataset provenance)
        Instant startedAt = Instant.now();

        // (1) Set RDF prefixes and (2) initialize required classes and properties
        BlogPostRDFConversion blogPostRDFConverter = new BlogPostRDFConversion();

        // 3. Convert a Link instance to RDF
        // Model aModel = blogPostRDFConverter.convertBlogPost((VoxBlogPost) testBlogPost);
        Model aModel = blogPostRDFConverter.convertBlogPost(blogScrapeVox.scrapePost(testURL2));

        // 4. Conversion process ends (used for dataset provenance), part of a separate named graph;
        /*
        Instant endedAt = Instant.now();
        aModel.add(blogPostRDFConverter.createProvenanceModel(startedAt, endedAt));
         */

        // 5. Store model: Write it to a file or store it in a triple store
        // blogPostRDFConverter.writeModelToFile("src/main/resources/blogPostInstance.ttl", aModel);
    }
}