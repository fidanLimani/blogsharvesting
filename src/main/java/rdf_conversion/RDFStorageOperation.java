package rdf_conversion;

import db.RDFStore;
import lombok.Data;
import org.apache.jena.rdf.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

import static constants.DBConstants.BLOG_POST_DATASET;
import static constants.RDFConversionConstants.BLOG_PROVENANCE_GRAPH;
import static constants.RDFConversionConstants.GRAPH_URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** This class brings together the features of RDFConverter and RDFStore: Given a Link instance, convert it and store it
 * to a triple store. */
@Data
public class RDFStorageOperation
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RDFStorageOperation.class);

    private static Model aModel;
    private RDFStore rdfStore;
    private BlogPostRDFConversion rdfConverter;

    // Specify the provenance of the link conversion and storage to RDF
    private Instant startedAt, endedAt;

    /** Default constructor */
    public RDFStorageOperation(String namedGraph) {
        rdfStore = new RDFStore(BLOG_POST_DATASET);
        rdfConverter = new BlogPostRDFConversion();
        rdfStore = new RDFStore(namedGraph);
    }

//    public void convertAndStore(AbstractLink linkInstance){
//        rdfConverter = new BlogPostToRDFConversion();
//        // Setup and convert the link to RDF
//        rdfConverter.setupPrefix();
//        rdfConverter.initializeLinkInstance();
//        aModel = rdfConverter.convertLink(linkInstance);
//
//        /* A link can have 1..N providers; for this, we need to store the same Link - thus RDF model - in as many named
//        graphs as there are link providers for a link. */
//        List<String> namedGraphs = linkInstance.getProviders();
//        for(String namedGraph : namedGraphs){
//            // Substitute eventual empty space with a dash ("-");
//            String uriNamedGraph = namedGraph.replace(" ", "-");
//
//            // Store the link to Jena TDB
//            rdfStore.storeModelToTDB(uriNamedGraph, aModel);
//        }
//    }

    /**
     * Harvest blog posts from a topic; convert them to RDF and store them in a triplestore.
     * @param blogTopic Include all the blog posts on a certain topic.
     **/
    /*
    public void rdfConvertTopicalBlogPosts(String blogTopic) {
        BlogHarvestStatus blogHarvestStatus;
        VoxBlogPost blogPostInstance;
        Model modelBatch = ModelFactory.createDefaultModel();
        BlogUtils blogUtils = blogScrapeVox.getBlogUtils();

        // Temp counter: Test the RDF conversion and JSON storage to file
        AtomicInteger blogPostCounter = new AtomicInteger();

        // Get the list of blog post URLs to scrape
        blogHarvestStatus = blogUtils.retrieveBlogHarvestStatus(blogTopic);

        Set<String> blogPostURLSet = blogHarvestStatus.getPostURLs();
        for (String blogPostURL : blogPostURLSet) {
            blogPostCounter.getAndIncrement();
            // System.out.printf(blogPostURL);
            blogPostInstance = blogScrapeVox.scrapePost(blogPostURL);

            // *** Convert all harvested blog posts to RDF: Apply batch processing due to DB write operations;
            modelBatch.add(blogPostRDFConversion.convertBlogPost(blogPostInstance));
            if (blogPostCounter.get() % BLOG_POST_CONVERSION_BATCH == 0) {
                LOGGER.info("A batch of 500 processed...");
                rdfStore.storeModelToTDB(String.format(GRAPH_URI, BLOG_POST_PREFIX), modelBatch);

                // Reset counter and (RDF) model
                modelBatch = ModelFactory.createDefaultModel();
                blogPostCounter.set(0);
            }

            // Introduce a pause in-between harvesting the different blog posts
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }

        // In this part add all the remaining Link instances that did not "make" a batch
        System.out.printf("\nStoring the %d remaining items after the last batch: \n", blogPostCounter.get());
        rdfStore.storeModelToTDB(String.format(GRAPH_URI, BLOG_POST_PREFIX), modelBatch);

        // Back up the contents of Jena TDB, then delete its contents to free up space
        LOGGER.info("Backing up and compressing the contents of the TDB...");
        rdfStore.backupAndCompressDataset();
    }
    */

    /**
     * Add provenance model(in RDF) and store it in a triplestore.
     *
     * Move this method to the rdf-conversion package #toDo
     **/
    /*
    public void rdfConvertProvenance() {
        LOGGER.info("Adding the provenance...");
        rdfStore.storeModelToTDB(String.format(GRAPH_URI, BLOG_PROVENANCE_GRAPH), blogPostRDFConversion.createProvenanceModel());

        // Back up the contents of Jena TDB, then delete its contents to free up space
        LOGGER.info("Backing up and compressing the contents of the TDB...");
        rdfStore.backupAndCompressDataset();
    }
     */
}