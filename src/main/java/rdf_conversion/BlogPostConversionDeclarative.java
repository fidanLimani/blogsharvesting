package rdf_conversion;

/*
import be.ugent.rml.Executor;
import be.ugent.rml.StrictMode;
import be.ugent.rml.Utils;
import be.ugent.rml.functions.FunctionLoader;
import be.ugent.rml.metadata.MetadataGenerator;
import be.ugent.rml.records.RecordsFactory;
import be.ugent.rml.store.QuadStore;
import be.ugent.rml.store.QuadStoreFactory;
import be.ugent.rml.store.RDF4JStore;
import be.ugent.rml.term.NamedNode;
 */
import utils.StringUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * A declarative conversion approach to RDF using the RDF Mapper engine
 * Adopted from the <ReadmeTest> class of the RML Mapper project
 *
 * NOTE: The libraries used for the RML mapping conflict with those for Maui indexer, hence this class has been
 * commented out.
 */
public class BlogPostConversionDeclarative {
//    private final String outputRDFFile = "./src/main/resources/declarative/rdfBlogPost.ttl";
//    private MetadataGenerator metadataGenerator;
//    final String DEFAULT_BASE_IRI = "http://example.com/base/";
//
//    // Convert a blog post instance to RDF
//    public void convertToRDF() {
//        try {
//            // Get the mapping string stream from the mapping file
//            String mapperFilePath = "./src/main/resources/declarative/mappings.rml.ttl";
//            File mappingFile = new File(mapperFilePath);
//            System.out.println("Mapping file parent:" + mappingFile.getParent());
//            InputStream mappingStream = new FileInputStream(mappingFile);
//
//            // Load the mapping in a QuadStore
//            QuadStore rmlStore = QuadStoreFactory.read(mappingStream);
//
//            // Set up the basepath for the records factory, i.e., the basepath for the (local file) data sources
//            RecordsFactory recordsFactory = new RecordsFactory(mappingFile.getParent());
//
//            // Set up the functions used during the mapping
//            Map<String, Class> libraryMap = new HashMap<>();
//            libraryMap.put("CustomFunctions", StringUtils.class);
//
//            // Set up the output store (needed when you want to output something else than n-quads
//            QuadStore outputStore = new RDF4JStore();
//
//            FunctionLoader functionLoader = new FunctionLoader(outputStore, libraryMap);
//
//            // Create the Executor
//            Executor executor = new Executor(rmlStore, recordsFactory, functionLoader, outputStore, Utils.getBaseDirectiveTurtle(mappingStream));
//            System.out.printf("Triples map size: %d \t Triples map element value: %s\n", executor.getTriplesMaps().size(), executor.getTriplesMaps().get(0).getValue());
//            QuadStore result = executor.executeV5(executor.getTriplesMaps()).get(new NamedNode("rmlmapper://default.store"));
//
//            // Create another Executor
//            /*
//            Executor executor2 = new Executor(rmlStore, recordsFactory, DEFAULT_BASE_IRI, StrictMode.BEST_EFFORT);
//            QuadStore result2 = executor2.executeV5(executor.getTriplesMaps()).get(new NamedNode("rmlmapper://default.store"));
//             */
//
//            // HashMap<Term, QuadStore> executeV5(List<Term> triplesMaps, boolean removeDuplicates, MetadataGenerator metadataGenerator)
//            /*
//            // Initialize the MetadataGenerator
//            String[] mappingFiles = {mapperFilePath};
//            QuadStore inputDataStore = new RDF4JStore();
//            metadataGenerator = new MetadataGenerator(MetadataGenerator.DETAIL_LEVEL.TRIPLE, outputRDFFile, mappingFiles, inputDataStore);
//
//            executor.executeV5(executor.getTriplesMaps(), true, metadataGenerator);
//             */
//
//            // Output the result
//            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));
//            //result.write(out, "turtle");
//            out.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    // Demo the app
//    public static void main(String[] args) {
//        BlogPostConversionDeclarative blogPostConversion = new BlogPostConversionDeclarative();
//        blogPostConversion.convertToRDF();
//
//    }
}
