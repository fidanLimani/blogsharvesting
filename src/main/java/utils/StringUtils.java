package utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static constants.RDFConversionConstants.BLOG_PROVENANCE_GRAPH;
import static constants.RDFConversionConstants.GRAPH_URI;

public class StringUtils {
    /**
     * List the key - value pairs of the Map<String, Integer>
     * @param aMap The input map
     */
    public static void displayStringIntMap(Map<String, Integer> aMap) {
        // Get keys and values
        for(Map.Entry<String, Integer> entry : aMap.entrySet()) {
            String topicStr = entry.getKey();
            int postCountInt = entry.getValue();
            System.out.println("[" + topicStr + " -> " + postCountInt + "]");
        }
    }

    /**
        Generate the URL for a given topic
        @param inputTopic Original topic label
        @return urlTopic  The URL representation of the URL
     */
    public static String generateTopicURL(String inputTopic) {
        return inputTopic.replaceAll("'", "")
                        .replaceAll(" ", "-")
                        .toLowerCase(Locale.ROOT);
    }

    public static LocalDate handleDateFormat(String inputDate) {
        LocalDate theDate;
        DateTimeFormatter formatter;

        // Example date: 20 October 2016
        formatter = DateTimeFormatter.ofPattern("dd MMMM y", Locale.ENGLISH);
        theDate = LocalDate.parse(inputDate, formatter);

        return theDate;
    }

    /**
     *  This method handles special characters in author names. Since we maintain separate URIs for authors, there is
     *  a need for legal URIs, and this requires that certain specific characters be dealt with.
     * @param originalName The source author name
     * @return The processed name value
     **/
    public static String handleAuthorName(String originalName) {
        String normalizedValue = Normalizer.normalize(originalName, Normalizer.Form.NFD);
        return normalizedValue.replaceAll("[^\\p{ASCII}]", "");
    }

    // Demo the class methods
    public static void main(String[] args) {
        String test = "View all 177 columns";
        System.out.println(test.replaceAll("[^0-9]", ""));

        String topicTest1 = "Financial markets";
        String topicTest2 = "Europe's nations and regions";
        System.out.println(topicTest1 + " -> " + generateTopicURL(topicTest1));
        System.out.println(topicTest2 + " -> " + generateTopicURL(topicTest2));

        // Instance vs LocalDate
        Instant startedAt = Instant.now();
        LocalDate localDate = LocalDate.now();
        System.out.println("Instance time example: " + startedAt);
        System.out.println("LocalDate time example:  " + localDate);
        System.out.println("LocalDate: " + LocalDate.ofInstant(startedAt, ZoneId.systemDefault()));

        String value = "Daron Acemoğlu";
        System.out.println("Normalized character: " + handleAuthorName(value));

        /*
        try {
            String nameValue = "Daron Acemoğlu";
            System.out.println("Encoding value: " + nameValue);
            System.out.println(URLEncoder.encode(nameValue, StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
         */
    }
}