package utils;

import blog_model.BlogHarvestStatus;
import blog_model.FeedEntry;

import blog_model.VoxBlogPost;
import constants.HarvestLoggingConstants;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import constants.BlogHarvestingConstants;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static constants.BlogHarvestingConstants.VOX_HARVEST_FILE;
import static constants.HarvestLoggingConstants.BLOG_POSTS_FILE;
import static constants.HarvestLoggingConstants.BLOG_POSTS_URLS;

/**
 * The methods required to IO files
 **/
public class FileIO {
    // Write a JSON object to a file
    private FileWriter fw;
    private BufferedWriter bw;

    public FileIO() {
        fw = null;
        bw = null;
        JSONParser jsonParser = new JSONParser();
    }

    public FileIO(String entryId) {
        fw = null;
        bw = null;
        JSONParser jsonParser = new JSONParser();
    }

    /**
     * This method creates the <.key> file required for training data in Maui indexer
     * @param topicTerms: Blog Post Tags
     * */
    public void createKey(File file, List<String> topicTerms){
        try {
            fw = new FileWriter(file.getAbsolutePath());
            bw = new BufferedWriter(fw);

            for(String temp : topicTerms){
                bw.write(temp + "\n");
            }

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method creates the <.txt> file
     * @param file The file (name)
     * @param title The title of the content
     * @param content The content of the file
     */
    private void createTextFile(File file, String title, String content) {
        try {
            fw = new FileWriter(file.getAbsolutePath());
            bw = new BufferedWriter(fw);
            bw.write(title);

            // Add a separation between the title and the blog post content.
            bw.write("\n");
            bw.write(content);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read a file line by line
     * We need this method to retrieve all the (a) blog posts or (b) all the URLs of the blog posts
     * @param fileName The file to read
     * */
    private void readTextFileByLine(String fileName) throws IOException {
        List<String> blogPostList = new LinkedList<>();
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        // blogPostList.add(reader.readLine());
        reader.lines().forEach(blogPostList::add); // reader.lines().forEach(line -> blogPostList.add(line));

        System.out.printf("Total blog posts in the file: %d", blogPostList.size());

        /*
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }

        return resultStringBuilder.toString();
        */
    }

    /**
     * This method creates a .txt file and writes the content of a JSONObject there
     * @param file The file (name)
     * @param blogPostObject The content of the blog post
     */
    public void writeJSONToFile(JSONObject blogPostObject, File file) {
        try {
            fw = new FileWriter(file, true);
            bw = new BufferedWriter(fw);
            bw.write(blogPostObject.toJSONString());
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method creates a .txt file and writes the content of a JSONObject there
     * @param items The item from the set
     * @param file The file (name)
     */
    public void writeSetStringToFile(Set<String> items, File file) {
        try {
            fw = new FileWriter(file, true);
            bw = new BufferedWriter(fw);

            for (String item : items) {
                bw.write(item);
                bw.newLine();
            }

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * This method creates the <.txt> file
     * @param feedEntry: A FeedEntry instance
     **/
    private void createJsonDocument(File file, FeedEntry feedEntry) throws IOException {
        System.out.println("Persist FeedEntry instance as a JSON document...");

        // Parse the XML into a FeedEntry instance
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(feedEntry);

        // Write JSON document to a test file
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(json);
        }
    }

    /* Create the two files for the training, and invoke the methods to fill them out (needed for Maui indexer)
     * @param: fileNumber The number to be used for the files generated;
     * @param: content Blog post title + author + content + publication date, concatenated;
     **/
    public void storeToFiles(FeedEntry entry){
        // The same <entryId> is going to be used for both .txt and .key files.
        String fileName = entry.getId();

        try {
            File txtFile = new File(String.format(BlogHarvestingConstants.FILE_STORAGE_PATH, BlogHarvestingConstants.TEXT_CORPUS) + fileName + ".txt");
            File keyFile = new File(String.format(BlogHarvestingConstants.FILE_STORAGE_PATH, BlogHarvestingConstants.TRAINING_CORPUS) + fileName + ".key");
            File jsonFile = new File(String.format(BlogHarvestingConstants.FILE_STORAGE_PATH, BlogHarvestingConstants.JSON_FILE) + fileName + ".json");

            if (txtFile.createNewFile() && keyFile.createNewFile() && jsonFile.createNewFile()){
                createTextFile(txtFile, entry.getTitle(), entry.getContent());
                createKey(keyFile, entry.getTopics());
                createJsonDocument(jsonFile, entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if file content is empty
     * @param file The file tot write to;
     * @return A boolean value denoting empty or non-empty file;
     **/
    public boolean isFileEmpty(File file) {
        return file.length() == 0;
    }

    /**
     * Append string to file contents
     */
    public static void appendToFile(String data, File file) throws IOException {
        FileWriter fr = new FileWriter(file, true);
        BufferedWriter br = new BufferedWriter(fr);
        br.write(data + "\n");

        br.close();
        fr.close();
    }
}