package utils;

import constants.BlogHarvestingConstants;
import org.apache.http.client.utils.URIBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

/* This class contains the methods to build a URI based for an RSS feed page, and send an HTTP request for it to the
* HBS service. */
public class RestCall {
    private URIBuilder uriBuilder;
    private URI hbsURI;
    private URL hbsURL;

    // Constructor
    public RestCall(){
        uriBuilder = new URIBuilder();
    }

    /* Build a URI based on HBS pattern, for a certain feed page. Another method is to use uriBuilder field to send the
    * HTTP request to HBS. */
    public void buildURI(int feedPage){
        this.uriBuilder.setScheme(BlogHarvestingConstants.SCHEME)
                        .setHost(BlogHarvestingConstants.HOST)
                        .setPath(BlogHarvestingConstants.PATH)
                        .setParameter(BlogHarvestingConstants.CID_PARAMETER, BlogHarvestingConstants.CID_VALUE)
                        .setParameter(BlogHarvestingConstants.PAGE_PARAMETER, String.valueOf(feedPage))
                        .setParameter(BlogHarvestingConstants.FQ_PARAMETER, BlogHarvestingConstants.FQ_VALUE)
                        .setParameter(BlogHarvestingConstants.FORMAT_PARAMETER, BlogHarvestingConstants.FORMAT_VALUE)
                        .setCharset(Charset.forName(BlogHarvestingConstants.CHARSET));

        try {
            hbsURI = uriBuilder.build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /* Send HTTP request to HBS
    * @return result The (HTML) HTTP response from HBS for a given RSS feed.
    * */
    public String getHttpRequest() throws IOException {
        StringBuilder result = new StringBuilder();
        this.hbsURL = new URL(hbsURI.toString());
        BufferedReader br = new BufferedReader(new InputStreamReader(hbsURL.openStream()));

        // Display the contents of the HBS feed page
        String inputLine;
        while ((inputLine = br.readLine()) != null)
            // Use StringBuilder instead of string concatenation
            result.append(inputLine).append("\n");
        br.close();

        return result.toString();
    }

    public static void main(String[] args) throws IOException {
        RestCall restCall = new RestCall();
        restCall.buildURI(2);
        String result = restCall.getHttpRequest();
        System.out.println(result);

        System.out.println("Application ended.");
    }
}