package utils;

import blog_model.*;

import blog_scrape.BlogScrapeVox;
import com.entopix.maui.filters.MauiFilter;
import com.entopix.maui.main.MauiWrapper;
import com.entopix.maui.util.Topic;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;

import static constants.BlogHarvestingConstants.*;
import static constants.HarvestLoggingConstants.BLOG_POSTS_FILE;

// This class contains the util features required for the blog harvesting
public class BlogUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlogUtils.class);
    private static MauiWrapper mauiWrapper;
    private final FileIO fileIO;
    private final JSONParser jsonParser;

    // Configure Maui model
    String modelName = "../blog-resources/src/main/resources/data/models/econBizModel.maui";
    String vocabularyName = "../blog-resources/src/main/resources/data/vocabularies/stw.rdf";
    String format = "skos";

    public BlogUtils() {
        fileIO = new FileIO();
        jsonParser = new JSONParser();

        mauiWrapper = new MauiWrapper(modelName, vocabularyName, format);
        mauiWrapper.setModelParameters(vocabularyName, null, null, null);
    }

    /**
     * Given a JSONObject instance for a blog topic of interest, update the current harvest statistics
     * @param inputJsonObject The object that contains the topic category information (BlogHarvestStatus type)
     **/
    private void updateJSONHarvestList(JSONObject inputJsonObject) {
        Map<String, JSONObject> blogHarvestMap = new HashMap<>();
        JSONObject tempObject;
        JSONArray tempJSONArray;
        try {
            // Store existing harvest statistics in a map for easier look up;
            JSONArray topicArray = (JSONArray) jsonParser.parse(new FileReader(VOX_HARVEST_FILE));
            for (Object o : topicArray) {
                tempObject = (JSONObject) o;
                blogHarvestMap.put((String) tempObject.get("topic"), tempObject);
            }

            FileWriter fileWriter = new FileWriter(VOX_HARVEST_FILE);
            tempJSONArray = new JSONArray();

            String targetTopic = (String) inputJsonObject.get("topic");
            if(blogHarvestMap.containsKey(targetTopic)) {
                // LOGGER.info("Topic <" + targetTopic + "> exist; check for updates!");
                tempObject = blogHarvestMap.get(targetTopic);

                // If the post count is different on the same topic, update
                // ((Number) obj).intValue() vs. Integer.parseInt(myObj)
                if (Integer.parseInt(tempObject.get("post count").toString()) !=
                                    Integer.parseInt(inputJsonObject.get("post count").toString())) {
                    tempObject.put("post count", inputJsonObject.get("post count"));
                    tempObject.put("postURLs", inputJsonObject.get("postURLs"));

                    // Update the objects map for the changed topic
                    blogHarvestMap.put(targetTopic, tempObject);
                }
            } else {
                blogHarvestMap.put((String) inputJsonObject.get("topic"), inputJsonObject);
            }

            // Update the JSON objects, including eventual changes, to the file
            blogHarvestMap.forEach((key, value) -> tempJSONArray.add(value));
            fileWriter.write(tempJSONArray.toJSONString());

            fileWriter.flush();
            fileWriter.close();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }


    /**
     * Given a blog topic, return all the harvest-related information for it (in a BlogHarvestStatus instance) present
     * in the storage (JSON) file.
     * @param blogTopic The blog topic of interest
     * @return blogHarvestStatus An instance of this class for a given topic
     **/
    public BlogHarvestStatus retrieveBlogHarvestStatus(String blogTopic) {
        BlogHarvestStatus blogHarvestStatus = new BlogHarvestStatus();
        JSONObject tempJsonObject;

        try (Reader reader = new FileReader(VOX_HARVEST_FILE)) {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);
            String tempObjectTopic;
            JSONArray tempJsonArray;
            Set<String> postURLs = new HashSet<>();

            for (JSONObject jsonObject : (Iterable<JSONObject>) jsonArray) {
                tempJsonObject = jsonObject;
                tempObjectTopic = (String) tempJsonObject.get("topic");

                // If a matching topic is found, instantiate the blogHarvestStatus object, and return it to the callee
                if (tempObjectTopic.equalsIgnoreCase(blogTopic)) {
                    blogHarvestStatus.setTopic(tempObjectTopic);
                    blogHarvestStatus.setBlogPostCount(Integer.parseInt(tempJsonObject.get("post count").toString()));
                    blogHarvestStatus.setTopicURL((String) tempJsonObject.get("topicURL"));

                    tempJsonArray = (JSONArray) tempJsonObject.get("postURLs");
                    tempJsonArray.forEach(element -> postURLs.add((String) element));

                    blogHarvestStatus.setPostURLs(postURLs);
                    // System.out.printf("There are %d posts URLs for the topic '%s': \n", postURLs.size(), blogTopic);
                    break;
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return blogHarvestStatus;
    }


    /**
     * Retrieve all the blog post URLs for a given a blog topic present in the (JSON) file. The blog topic, all the URL
     * blog posts - all stored in a JSON file.
     * @param blogTopic The blog topic of interest
     * @return postURLs The URLs from a given topic
     **/
    public Set<String> retrievePostURLs(String blogTopic) {
        Set<String> postURLs = new HashSet<>();
        JSONObject tempJsonObject;

        try (Reader reader = new FileReader(VOX_HARVEST_FILE)) {
            JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);
            String tempObjectTopic;
            JSONArray tempJsonArray;

            for (JSONObject jsonObject : (Iterable<JSONObject>) jsonArray) {
                tempJsonObject = jsonObject;
                tempObjectTopic = (String) tempJsonObject.get("topic");

                // If a matching topic is found, instantiate the blogHarvestStatus object, and return it to the callee
                if (tempObjectTopic.equalsIgnoreCase(blogTopic)) {
                    tempJsonArray = (JSONArray) tempJsonObject.get("postURLs");
                    tempJsonArray.forEach(element -> postURLs.add((String) element));

                    System.out.printf("There are %d posts URLs for the topic '%s': \n", postURLs.size(), blogTopic);
                    break;
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return postURLs;
    }


    /**
     * Extract the end range of a pagination for a topic
     * @param paginationRange The string representing the pagination information, for e.g.; "1 of 9";
     * @return The end range of the pagination information. From the example, this would be the number 9;
     **/
    public int extractMaxPage(String paginationRange) {
        return Integer.parseInt((String) Arrays.stream(paginationRange.split(" ")).toArray()[2]);
    }

    /***
     * Store individual topic-level statistics as JSON objects in a (JSON) file. This is the first step to harvesting
     * the blog post collections.
     * @param blogHarvestStatus An instance with the topic-level statistics of a specific topic
     */
    public void storeBlogHarvestToJSON(BlogHarvestStatus blogHarvestStatus) {
        File outputFile = new File(VOX_HARVEST_FILE);

        try {
            // If the destination JSON file (i.e., VOX_HARVEST_FILE) is empty, "initiate" it to an empty JSON array
            if (fileIO.isFileEmpty(outputFile)) {
                FileWriter fileWriter = new FileWriter(outputFile);
                fileWriter.write("[]");
                fileWriter.flush();
                // System.out.println("JSON file initialized to an empty array.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Add all blog post URLs of a topic/category
        JSONArray postURLs = new JSONArray();
        postURLs.addAll(blogHarvestStatus.getPostURLs());

        // Add the other elements
        JSONObject topicStatsInstance = new JSONObject();
        topicStatsInstance.put("topic", blogHarvestStatus.getTopic());
        topicStatsInstance.put("topicURL", blogHarvestStatus.getTopicURL());
        topicStatsInstance.put("post count", blogHarvestStatus.getBlogPostCount());
        topicStatsInstance.put("postURLs", postURLs);

        // Enter/Update the topics list, as required
        updateJSONHarvestList(topicStatsInstance);
    }

    /***
     * Store individual VoxBlogPost instance as JSON objects in a text file.
     * @param blogPostInstance A Vox blog post instance
     * @param fileName Name of the file to store the harvested blog posts to.
     */
    public void storeVoxBlogPostToJSON(VoxBlogPost blogPostInstance, String fileName) {
        JSONObject blogPostObject = new JSONObject();
        blogPostObject.put("uuid", blogPostInstance.getUuid());
        blogPostObject.put("url", blogPostInstance.getURL());
        blogPostObject.put("title", blogPostInstance.getTitle());

        List<String> blogPostAuthors = blogPostInstance.getAuthors();
        if (blogPostAuthors != null && !blogPostAuthors.isEmpty()) {
            JSONArray authorList = new JSONArray();
            authorList.addAll(blogPostAuthors);
            blogPostObject.put("authors", authorList);
        }

        blogPostObject.put("local date", blogPostInstance.getPublicationDate());
        blogPostObject.put("body", blogPostInstance.getBody());

        // For some blog posts, there could be different types of blog posts (other than "text" or "podcast" enum types)
        if(blogPostInstance.getBlogPostType() != null) {
            blogPostObject.put("type", blogPostInstance.getBlogPostType());
        }

        blogPostObject.put("summary", blogPostInstance.getSummary());

        List<String> referencesList = blogPostInstance.getReferences();
        if (referencesList != null) {
            JSONArray jsonReferenceList = new JSONArray();
            jsonReferenceList.addAll(referencesList);
            blogPostObject.put("references", jsonReferenceList);
        }

        // Add the topic terms
        JSONArray topicList = new JSONArray();
        JSONObject topicObject;

        // For some blog post, there are no topics associated with a post. Check for a null-pointer exception!
        List<VoxTopic> theTopicList = blogPostInstance.getTopics();
        if(theTopicList != null) {
            for (VoxTopic topic : theTopicList) {
            /* Since there are a few topics/tags per post, it is more efficient to create a new JSONObject
            than "reset" its values from a previous instance. */
                topicObject = new JSONObject();
                topicObject.put("topic label", topic.getCategoryLabel());
                topicObject.put("topic URL", topic.getTopicURL());
                topicList.add(topicObject);
            }
        }

        blogPostObject.put("topics", topicList);

        // Add the tag terms
        JSONArray tagList = new JSONArray();
        JSONObject tagObject;

        // For some blog post, there are no topics associated with a post. Check for a null-pointer exception!
        List<VoxTag> theTagList = blogPostInstance.getTags();
        if(theTagList != null) {
            for (VoxTag tag : theTagList) {
            /* Since there are a few topics/tags per post, it is more efficient to create a new JSONObject
            than "reset" its values from a previous instance. */
                tagObject = new JSONObject();
                tagObject.put("tag label", tag.getCategoryLabel());
                tagObject.put("tag URL", tag.getTagURL());
                tagList.add(tagObject);
            }
        }

        blogPostObject.put("tags", tagList);

        // Finally, store the automatically generated terms based on the STW thesaurus
        List<STWTerm> stwTermsList = blogPostInstance.getStwTerms();
        if (stwTermsList != null) {
            JSONArray stwTermsJsonArray = new JSONArray();
            JSONObject stwTermObject;
            for (STWTerm stwTerm : stwTermsList) {
            /* Since there are a few topics/tags per post, it is more efficient to create a new JSONObject
            than "reset" its values from a previous instance. */
                stwTermObject = new JSONObject();
                stwTermObject.put("stwTermLabel", stwTerm.getCategoryLabel());
                stwTermObject.put("stwTermURL", stwTerm.getStwTermURL());
                stwTermsJsonArray.add(stwTermObject);
            }

            blogPostObject.put("stwTerms", stwTermsJsonArray);
        }

        // Write the resulting blog post (in the JSON object) to the file
        // v.01 Store the blog posts across topics in a single file?
        // v0.2 Store blog posts in files based on the topic
        // File outputFile = new File(String.format(BLOG_TOPIC_FILES, fileName.toLowerCase().replace(" ", "-")));
        fileIO.writeJSONToFile(blogPostObject, new File(fileName));
    }

    /**
     * Extract Topics from an Elements list.
     * @param elements A list of <a> elements that contain both "topics" and "terms"
     * @return topic instance
     */
    public List<AbstractTopicCategory> retrieveTopics(Elements elements) {
        List<AbstractTopicCategory> categoryList = new LinkedList<>();

        for(Element e : elements) {
            String anchorLabel = e.text();
            String anchorURL = e.attr(VOX_ARTICLE_TOPIC_LINK);
            // System.out.printf("Label: %s \t Label URL: %s\n", anchorLabel, anchorURL);

            if(anchorURL.contains("topics")) {
                // System.out.println("It is a topic.");
                categoryList.add(new VoxTopic(String.format(VOX_BASE_URL, anchorURL), anchorLabel));
            }

            if(anchorURL.contains("term") || anchorURL.contains("tag")) {
                // System.out.println("It is a term.");
                categoryList.add(new VoxTag(String.format(VOX_BASE_URL, anchorURL), anchorLabel));
            }
        }

        return categoryList;
    }

    // Retrieve (up to 3) STW terms from a blog post
    public List<STWTerm> assignStwTerms(String text, int termsCount) {
        List<Topic> stwTopics;
        List<STWTerm> stwTerms = new LinkedList<>();
        try {
            stwTopics = mauiWrapper.extractTopicsFromText(text, termsCount);

            for (Topic topic : stwTopics) {
                stwTerms.add(new STWTerm(topic.getId(), topic.getTitle()));
            }
        } catch (MauiFilter.MauiFilterException e) {
            e.printStackTrace();
        }

        return stwTerms;
    }

    /**
     * A method to test a NPE thrown during adding string elements from "Macroeconomic policy" policy to a Set<String>
     * */
    private void testTopic () {
        // All de-duplicated post URLs
        Set<String> stringSet = new HashSet<>();
        int blogPostCount = 0;
        int blogPostCountPerTopic;

        // Retrieve the blog topics
        BlogScrapeVox blogScrapeVox = new BlogScrapeVox();
        List<String> blogTopics = blogScrapeVox.scrapeBlogTopics();

        Set<String> tempSet;
        for (String blogTopic : blogTopics) {
            tempSet = retrieveBlogHarvestStatus(blogTopic).getPostURLs();

            // Some
            if (tempSet != null) {
                stringSet.addAll(tempSet);
                blogPostCountPerTopic = retrieveBlogHarvestStatus(blogTopic).getBlogPostCount();

                // Keep a running total
                blogPostCount += blogPostCountPerTopic;

                System.out.printf("%s: %d\n", blogTopic, blogPostCountPerTopic);
                System.out.printf("Cumulative blog post count: %d \t Unique blog posts so far: %d\n\n", blogPostCount, stringSet.size());
            } else {
                System.out.printf("There seems to be a NPE with the topic %s", blogTopic);
            }
        }

        System.out.printf("# of blog topics: %d", blogTopics.size());
    }

    // Test the app
    public static void main(String[] args) {
        BlogUtils blogUtils = new BlogUtils();
        VoxBlogPost aBlogPost = (VoxBlogPost) VoxBlogPost.generateTestBlogPost();
        String fileName = "src/main/resources/blogs/aTestPost.json";
        blogUtils.storeVoxBlogPostToJSON(aBlogPost, fileName);
    }
}