package utils;

import enums.HBSContentType;
import blog_model.FeedEntry;

import java.io.IOException;

import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class parses XML and cretes files
 */
public class FeedUtilities {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedUtilities.class);

    private SAXBuilder saxBuilder;

    // Constructor
    public FeedUtilities() {
        this.saxBuilder = new SAXBuilder();
    }

    /**
     * This method parsers the response from the HBS feed response, which includes details to the extent applicable for
     * an Atom feed.
     */
    private List<FeedEntry> getFeedEntries(String feedContent) {
        List<FeedEntry> feedEntries = new LinkedList<>();
        try {
            Document document = saxBuilder.build(new StringReader(feedContent));

            // Get the root element of the XML response, i.e. <feed>
            Element rootNode = document.getRootElement();
            LOGGER.info("Root element: " + rootNode.getName());

            // Get all the <entry> nodes
            List entryNodeList = rootNode.getChildren("entry");
            System.out.println("# of feed entries on this page: " + entryNodeList.size());

            FeedEntry tempFeedEntry;

            LOGGER.info("Processing <entry> nodes...");
            for (Object node : entryNodeList) {
                // Create FeedEntry instance based on entries from the List<Element>
                tempFeedEntry = new FeedEntry();

                // Get the <entry> element from the list
                Element element = (Element) node;

                String id = element.getChildText("id");
                System.out.println("Entry id: " + id);
                tempFeedEntry.setId(id);

                String title = element.getChildText("title");
                System.out.println("Entry title: " + title);
                tempFeedEntry.setTitle(title);

                // Retrieve date published and updated
                String strDatePublished = element.getChildText("published");
                LocalDate datePublished = stringToDate(strDatePublished);
                System.out.println("Date published: " + datePublished);
                tempFeedEntry.setDatePublished(datePublished);

                String strDateUpdated = element.getChildText("updated");
                LocalDate dateUpdated = stringToDate(strDateUpdated);
                System.out.println("Date updated: " + dateUpdated);
                tempFeedEntry.setDateUpdated(dateUpdated);

                String link = element.getChild("link").getAttributeValue("href");
                System.out.println("Entry link: " + link);
                tempFeedEntry.setLink(link);

                String summary = element.getChildText("summary");
                tempFeedEntry.setSummary(summary);

                // Retrieve the "content" of the blog post
                String htmlContent = element.getChildText("content");
                org.jsoup.nodes.Document content = Jsoup.parse(htmlContent);
                tempFeedEntry.setContent(content.text());
                System.out.println("Blog post content:\n" + content.text());

                /* Retrieve nested elements: authors, sources, topics, locations, industries */
                List<String> authors = elementToString(element.getChild("author"), "name");
                tempFeedEntry.setAuthors(authors);

                List<String> sources = elementToString(element.getChild("source"), "title");
                tempFeedEntry.setSources(sources);

                List<String> topics = elementToString(element.getChild("topics"), "topic");
                tempFeedEntry.setTopics(topics);

                List<String> locations = elementToString(element.getChild("locations"), "location");
                tempFeedEntry.setLocations(locations);

                List<String> industries = elementToString(element.getChild("industries"), "industry");
                tempFeedEntry.setIndustries(industries);

                // Content type: Specify "xmln" attributes value in getChildText("childName", "nsValue")
                String contentType = element.getChildText("contenttype", Namespace.getNamespace("http://www.hbs.edu/"));
                System.out.println("Entry content type: " + contentType);
                // The HBS XML response often contains space that needs to be trimmed to "match" ContentType enum
                tempFeedEntry.setContentType(HBSContentType.valueOf(contentType.trim()));


                // HBS publication date: same approach as with <contenttype>. This element is not required for FeedEntry.
                String hbsDate = element.getChildText("hbsdate", Namespace.getNamespace("http://www.hbs.edu/"));
                System.out.println("HBS date: " + hbsDate);

                // Add a FeedEntry instance to the list
                feedEntries.add(tempFeedEntry);
            }
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }

        return feedEntries;
    }

    /* This method retrieves nested elements from the XML feed.
    * Elements to process with this method (any of the following could be empty):
        - authors -> (author, name)
        - sources -> (source, title)
        - topics -> (topics, topic)
        - locations -> (locations, location)
        - industries -> (industries, industry)
    **/
    private List<String> elementToString(Element targetElement, String elementName) {
        List<String> elements = new LinkedList<>();

        // Get all the author <name> nodes
        List entryNodeList = targetElement.getChildren(elementName);

        // INDICATOR code, to be removed !!!
        System.out.println("Child nodes for " + targetElement.getName() + ": " + entryNodeList.size());

        // Some elements might be null. Handle this case!
        LOGGER.info("Processing " + targetElement.getName() + "...\n");

        if (!entryNodeList.isEmpty()) {
            for (Object node : entryNodeList) {
                Element childElement = (Element) node;
                elements.add(childElement.getText());
                System.out.println(targetElement.getName() + ": " + childElement.getText()
                        .replace("by", "")
                        .replace("re:", "")
                        .replace("Re:", "")
                        .trim());
            }

            return elements;
        }

        // Information for <targetElement> are not present
        return Collections.emptyList();
    }

    // String to LocalDate
    private LocalDate stringToDate(String date) {
        Instant instant = Instant.parse(date);
        LocalDateTime result = LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));

        return result.toLocalDate();
    }

    // Read the test XML file content
    private String readTestXmlFile() {
        StringBuilder results = new StringBuilder();
        Path xmlFile = Paths.get("C:\\Users\\limani fidan\\GitHub\\econbizandhbs\\src\\main\\resources\\outputExamples\\",
                "HBSdetailsPAge.xml");
        Charset charset = StandardCharsets.UTF_8;

        try {
            List<String> lines = Files.readAllLines(xmlFile, charset);

            for (String line : lines)
                results.append(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results.toString();
    }

    /*
    * Assign STW-bsaed terms to every HBS blog post
    *
    * a) Consider training a MAUI model with harvested HBS blog posts
    * b) Assign STW terms to each blog post (- the blog posts used for training)
    *
    * Note: Training should include HBS blo posts described wit STW terms, and this is not available!!!
    * How to proceed?
    **/

    // Demo the app
    public static void main(String[] args) throws IOException {
        FileIO fileGeneration = new FileIO();

        // Read the contents of an XML example file, such as "HbsDetailsPage.xml"
        FeedUtilities feedUtilities = new FeedUtilities();
        String testXmlContent = feedUtilities.readTestXmlFile(); // Test data
        List<FeedEntry> feedEntries = feedUtilities.getFeedEntries(testXmlContent);

        /* Store blog post to a file (.txt and .key)
        for(FeedEntry entry : feedEntries){
            System.out.println("Storing the following file in .txt and .key: " + entry.getId());
            fileGeneration.storeToFiles(entry);
        }
        */

        /*
        System.out.println("Making Rest HTTP request to HBS...");
        RestCall restCall = new RestCall();
        restCall.buildURI(2);

        System.out.println("Converting the HBS reponse (in XML) to FeedEntry...");
        feedUtilities.getFeedEntries(restCall.getHttpRequest());
        */
    }
}