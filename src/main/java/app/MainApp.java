package app;

import harvesters.VoxHarvester;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;

/**
 * This class brings together the classes and method calls required to parse, convert, and store harvested resources.
 **/
public class MainApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainApp.class);
    private final VoxHarvester voxHarvester;

    public MainApp() {
        voxHarvester = new VoxHarvester();
    }

    public void runApp() {
        voxHarvester.harvestBlog();
    }

    // Run the app
    public static void main(String[] args){
        MainApp mainApp = new MainApp();
        LOGGER.info("Harvesting the VoxEU blog has started...");
        mainApp.runApp();
        LOGGER.info("Application completed successfully.");
    }
}