package enums;

// import lombok.Data;

// Provide the HBS content types
public enum HBSContentType {
    // HBS content types
    Book("Book"),
    ColdCallPodcast("Cold Call Podcast"),
    ExecutiveEducation("Executive Education"),
    FirstLook("First Look"),
    HBSCase("HBS Case"),
    LessonsFromTheClassroom("Lessons from the Classroom"),
    OpEd("Op-Ed"),
    ResearchAndIdeas("Research & Ideas"),
    ResearchEvent("Research Event"),
    SharpeningYourSkills("Sharpening Your Skills"),
    WhatDoYouThink("What Do You Think?"),
    WorkingPaperSummaries("Working Paper Summaries");

    private String text;

    /** @param text String representation of an HBS content type */
    HBSContentType(final String text) {
        this.text = text;
    }

    // A getter for the field
    private String getText(){
        return this.text;
    }

    // A setter for the field
    private void setText(String text){
        this.text = text;
    }
}