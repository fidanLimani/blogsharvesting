package enums;

// Filter Vox.eu posts by a category: topic, date (archive), reads, and tag
public enum VoxHarvestType {
    Topic("topics"),
    Archive("archive"),
    Reads("reads"),
    Tag("tag");

    private String text;

    /** @param text String representation of an Vox.eu category to filter posts by */
    VoxHarvestType(final String text) {
        this.text = text;
    }

    // A getter for the field
    public String getText(){ return this.text; }

    // A setter for the field
    private void setText(String text){
        this.text = text;
    }
}