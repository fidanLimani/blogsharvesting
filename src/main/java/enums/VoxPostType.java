package enums;

// There are 2 Vox post types: Text-based, and podcasts.
public enum VoxPostType {
        Text("text"),
        Podcast("podcast");

        private String text;

        /** @param text String representation of an Vox.eu category to filter posts by */
        VoxPostType(final String text) {
            this.text = text;
        }

        // A getter for the field
        public String getText(){ return this.text; }

        // A setter for the field
        private void setText(String text){
            this.text = text;
        }
}