package harvesters;

// The interface for all blog harvesters
public interface BlogHarvester {
    void harvestBlogCollectionFromURL(String baseURL);
    void harvestBlogCollection();
}