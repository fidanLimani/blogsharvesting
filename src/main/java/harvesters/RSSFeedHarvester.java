package harvesters;

/*
* Approach
*
* - Send an HTTP Get request to HBS Atom feed, specifying the page;
* - Parse the (XML) response
* - For each entry item in the response:
*   a. Create a FeedEntry instance
*   b. Persist the instance
*       1. To file: Title + Content as both .txt and .jey format. This is to be used by MAUI later on.
*       2. The complete instance content, as a JSON document, to be stored in a document store (Mongo DB)
*           We can always decide to index these document, depending on the application/prototype requirements
* */

public class RSSFeedHarvester {

    public RSSFeedHarvester(){

    }

    public static void main(String[] args) {
        RSSFeedHarvester harvester = new RSSFeedHarvester();
    }
}