package harvesters;

import lombok.Data;

import blog_model.VoxBlogPost;
import blog_scrape.BlogScrapeVox;

import static constants.HarvestLoggingConstants.BLOG_POSTS_FILE;
import static constants.HarvestLoggingConstants.BLOG_POSTS_URLS;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.FileIO;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Data
public class VoxHarvester {
    private static final Logger LOGGER = LoggerFactory.getLogger(VoxHarvester.class);
    private BlogScrapeVox blogScrapeVox;
    private Set<String> blogPostURLSet;
    private FileIO fileIO;
    private ProgressBarBuilder progressBarBuilder;

    // Default constructor
    public VoxHarvester() {
        blogScrapeVox = new BlogScrapeVox();
        blogPostURLSet = new HashSet<>();
        fileIO = new FileIO();

        // Setup the ProgressBars
        progressBarBuilder = new ProgressBarBuilder()
                .setTaskName("Reading")
                .setUnit("MB", 1048576)
                .setStyle(ProgressBarStyle.COLORFUL_UNICODE_BLOCK);
    }

    /**
     * What are all the blog posts VoxEU to be harvested? Return a set of these blogs.
     **/
    public void harvestBlog() {
        // 1. Retrieve the list of topics for the blog
        List<String> blogPostTopics = blogScrapeVox.scrapeBlogTopics();

        /* 2. Store all the posts from each blog topic: Make sure that the posts that appear in > 1 topic are only
            stored once */
        for (String blogPostTopic : blogPostTopics) {
            LOGGER.info("Retrieving the blog posts on the topic of '" + blogPostTopic + "'");
            // Add all the blog post URLs from the current blog topic to the set

            blogPostURLSet.addAll(blogScrapeVox.scrapeBlogPostURLsFromTopic(blogPostTopic));
            LOGGER.info("Blog post list size: " + blogPostURLSet.size());

            // Introduce a pause in-between harvesting the different blog topics
            try {
                TimeUnit.SECONDS.sleep(30);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }

        // Display some stats about the resulting blog posts to harvest
        LOGGER.info("Total number of blog posts to harvest: " + blogPostURLSet.size());
        fileIO.writeSetStringToFile(blogPostURLSet, new File(BLOG_POSTS_URLS));

        LOGGER.info("Starting the blog post harvest... ");
        harvestBlogPosts(blogPostURLSet, BLOG_POSTS_FILE);
    }

    /**
     * Harvest all the blog posts from a given topic of VoxEU, and store its blog posts as JSON objects in a file.
     **/
    public void harvestBlogPosts (Set<String> blogPostURLs, String destinationFile) {
        // Get all the blogs from a topic
        // Set<String> blogPostURLs = blogScrapeVox.scrapeTopicPosts(blogTopic);
        VoxBlogPost aBlogPost;

        // Scrape all the blog posts from the list, and store them as JSON objects in a (text) file
        for (String blogPostURL : ProgressBar.wrap(blogPostURLs, "Harvesting...")) {
            aBlogPost = blogScrapeVox.scrapePost(blogPostURL);
            blogScrapeVox.getBlogUtils().storeVoxBlogPostToJSON(aBlogPost, destinationFile);

            // Introduce a pause in-between harvesting the different blog posts
            try {
                TimeUnit.SECONDS.sleep(7);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
        }
    }
}