package constants;

/**
 * This static class contains constants used for the DB-related operations
 * @author Fidan Limani
 */

public class DBConstants {
    // This value is used to set the Jena TDB dataset to contain the ScholExplorer link collection
    public static final String BLOG_POST_DATASET = "blogPosts";
}