package constants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class BlogHarvestingConstants {
    // Path to files to read from and write to
    public static final String FILE_STORAGE_PATH = "econbizandhbs/src/main/resources/fileCollection/%s/";
    public static final String JSON_FILE = "jsonFiles";
    public static final String TEXT_CORPUS = "textFiles";
    public static final String TRAINING_CORPUS = "keyFiles";

    public static final String SCHEME = "http";
    public static final String HOST = "hbswk.hbs.edu";
    public static final String PATH = "/Pages/browse.aspx";
    public static final String PAGE_PARAMETER = "page";
    public static final String CID_PARAMETER = "cid";
    public static final String CID_VALUE = "rss-wk-zbw";
    public static final String FQ_PARAMETER = "fq";
    public static final String FQ_VALUE = encodeValue();
    public static final String FORMAT_PARAMETER = "_format";
    public static final String FORMAT_VALUE = "atom";
    public static final String CHARSET = "UTF-8";

    public static final String FEED_ENTRY_WORK_ID = "wk-%d";

    // Blog: Vox.eu
    public static final String VOX_EU_URL_BASE = "https://voxeu.org/columns/%s";
    public static final String VOX_EU_URL_TOPIC = "https://voxeu.org/content/topics/%s";
    public static final String BLOG_PAGE = "?page=%d";
    public static final String VOX_BASE_URL = "https://voxeu.org%s";
    public static final String VOX_HARVEST_FILE = "src/main/resources/blogs/harvestStats.json";
    public static final String HTTP_PROTOCOL = "https://%s";
    // Assing <= 3 terms via Maui indexer
    public static final int TERM_ASSIGNMENT_COUNT = 3;

    // CSS rules used for Web scraping
    public static final String TOPIC_SELECTOR = "h2.field-content";
    public static final String COUNT_SELECTOR_TEXT = "span.author-column-count";
    public static final String VOX_POST_PODCAST = "img.title-icon.podcast-icon";
    public static final String VOX_POST_TITLE = "h1.article-title > p";
    // Some blog posts have a different rule for the title :(
    public static final String VOX_POST_TITLE_V02 = "h1.article-title";
    public static final String VOX_POST_PODCAST_TITLE = "h1.article-title";
    public static final String VOX_POST_AUTHORS = "div.views-field.views-field-nothing span > a";
    public static final String VOX_PUBLICATION_DATE = "span.date-display-single";
    // "div.article-teaser > p" used to work for most of the blog posts, but there were those with a slightly different "rule"
    public static final String VOX_ARTICLE_SUMMARY = "div.article-teaser p";
    public static final String VOX_ARTICLE_CONTENT = "div.article-content";
    public static final String VOX_ARTICLE_REFERENCE_ELEMENT = "h2:contains(References)";
    public static final String VOX_ARTICLE_REFERENCE_ITEMS = ".article-content > *:not(:contains(References) ~ *)";
    public static final String VOX_ARTICLE_ENDNOTE_ELEMENT = "h2:contains(Endnotes)";
    public static final String VOX_ARTICLE_ENDNOTE_ITEMS = ".article-content > *:contains(Endnotes) ~ p";
    public static final String VOX_ARTICLE_TABLE_ELEMENT = ".article-content > table *";
    public static final String VOX_ARTICLE_CONTENT_ITEMS = ".article-content > *";
    public static final String VOX_ARTICLE_TOPICS = "span.field-label ~ a";
    public static final String VOX_ARTICLE_TOPIC_LINK = "href";

    // Blog: EconLib
    public static final String ECON_LIB_URL_BASE = "https://www.econlib.org/%s";

    /*
    * Provide URL encoding for a parameter value
    * */
    private static String encodeValue(){
        String encodedResult = "";
        try {
            encodedResult = URLEncoder.encode("-HBSContentType:First*", StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedResult;
    }
}