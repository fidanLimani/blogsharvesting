package constants;

/**
 * This class contains the constants required during blog post harvest.
 * */
public class HarvestLoggingConstants {
    // Blog names
    public static final String VOX_EU_BLOG = "VoxEU Blog";
    public static final String HBS_BLOG = "Harvard Business School Blog";
    public static final String ECON_LIB_BLOG = "EconLib Blog";

    // Blog URLs
    public static final String  VOX_EU_HARVEST = "Harvesting the VoxEU blog";
    public static final String  HBS_HARVEST = "Harvesting the Harvard Business School blog";
    public static final String  ECON_LIB_HARVEST = "Harvesting the EconLib blog";

    public static final String RDF_DATASET_CREATION = "Dataset %s created.";
    public static final String FILE_CREATION = "Writing to %s.";
    // Provide the file name for the text file to be created
    public static final String BLOG_POSTS_FILE = "src/main/resources/blogs/allPosts.txt";
    public static final String BLOG_POSTS_URLS = "src/main/resources/blogs/allPostURLs.txt";


    public static final int BLOG_POST_CONVERSION_BATCH = 500;
}