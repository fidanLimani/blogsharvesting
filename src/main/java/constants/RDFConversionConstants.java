package constants;

// THis class contains the constants required for RDF modeling, and this mainly includes the namespace-related info.
public class RDFConversionConstants
{
    // Namespace definitions for the model
    public static final String BASE_URI = "http://zbw.eu/scholarly-resource/%s/";

    // Namespace prefixes specific to the Scholarly Graph
    public static final String GRAPH_PREFIX = "graph/%s";
    public static final String GRAPH_URI = String.format(BASE_URI, GRAPH_PREFIX);

    public static final String BLOG_POST_PREFIX = "blog-post";
    public static final String BLOG_POST_URI = String.format(BASE_URI, BLOG_POST_PREFIX);

    // Used to create (primary || alternate) identifier instances
    public static final String IDENTIFIER_PREFIX = "identifier";
    public static final String IDENTIFIER_URI = String.format(BASE_URI, IDENTIFIER_PREFIX);

    // Used for artifact authors
    public static final String AUTHOR_PREFIX = "author";
    public static final String AUTHOR_URI = String.format(BASE_URI, AUTHOR_PREFIX);

    // Used for artifact topic / subject (wherever applicable) (Additional to the STW-based representation)
    public static final String TOPIC_PREFIX = "topic";
    public static final String TOPIC_URL = String.format(BASE_URI, TOPIC_PREFIX);

    // Provenance
    public static final String PROVENANCE_PREFIX = "provenance";
    public static final String PROVENANCE_URI = String.format(BASE_URI, PROVENANCE_PREFIX);

    // SIOC: The key entity - blogs
    public static final String SIOC_PREFIX = "sioc";
    public static final String SIOC_URI = "http://rdfs.org/sioc/ns#";

    // Schema.org: Model the keywords
    public static final String SCHEMA_PREFIX = "schema";
    public static final String SCHEMA_URI = "http://schema.org/";

    // Namespace prefixes for the ontologies and vocabularies uesd to model Scholix Framework links
    public static final String DC_TERMS_PREFIX = "dcterms";
    public static final String DC_TERMS = "http://purl.org/dc/terms/";

    public static final String FOAF_PREFIX = "foaf";
    public static final String FOAF = "http://xmlns.com/foaf/0.1/";

    public static final String RDF_PREFIX = "rdf";
    // Having it as "RDF" conflicts with the org.apache.jena.vocabulary.RDF class, hence the "URI" addition;
    public static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

    public static final String RDFS_PREFIX = "rdfs";
    public static final String RDFS_URI = "http://www.w3.org/2000/01/rdf-schema#";

    public static final String XSD_PREFIX = "xsd";
    public static final String XSD = "http://www.w3.org/2001/XMLSchema#";

    public static final String PROV_PREFIX = "prov";
    public static final String PROV = "http://www.w3.org/ns/prov#";

    public static final String CC_PREFIX = "cc";
    public static final String CC = "http://creativecommons.org/ns#";

    public static final String LITERAL_PREFIX = "literal";
    public static final String LITERAL = "http://www.essepuntato.it/2010/06/literalreification/";

    public static final String PROV_O_PREFIX = "provenance";
    public static final String PROV_O_URI = String.format(BASE_URI, PROV_O_PREFIX);

    // Model values for classes and properties (All these values should be represented as constants!)
    // Classes
    public static final String BLOG_POST = "BlogPost";
    public static final String PROV_COLLECTION_CLASS = "Collection";
    public static final String PROV_GENERATION_CLASS = "Generation";
    public static final String PROV_ACTIVITY_CLASS = "Activity";
    public static final String PROV_SOFTWARE_AGENT_CLASS = "SoftwareAgent";

    // Properties
    public static final String PROV_ACTIVITY_START_TIME_VALUE = "startedAtTime";
    public static final String PROV_ACTIVITY_END_TIME_VALUE = "endedAtTime";
    public static final String PROV_USED_VALUE = "used";
    public static final String PROV_GENERATED_VALUE = "generated";
    public static final String PROV_WAS_GENERATED_BY_VALUE = "wasGeneratedBy";
    public static final String PROV_QUALIFIED_GENERATION_VALUE = "qualifiedGeneration";

    public static final String SCHEMA_KEYWORDS = "keywords";
    public static final String SIOC_CONTENT = "content";

    // Constant literals
    // License: Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
    public static final String LICENSE_URL_BY_NC = "https://creativecommons.org/licenses/by-nc/4.0/";
    public static final String BLOG_PROVENANCE_GRAPH = "blog-provenance";
    public static final String VOXEU_COLLECTION_TITLE = "VoxEU";
    public static final String PROV_GENERATION_COMMENT_VALUE = "RDF representation of a blog post collection.";
    public static final String ORIGINAL_COLLECTION_FORMAT = "application/x-tar";
    public static final String RDF_COLLECTION_INSTANCE = "%s-blog-post-collection-rdf";
    public static final String BLOG_CONVERSION_VALUE = "%s-collection-conversion-activity";
    public static final String PROV_GENERATION_INSTANCE_VALUE = "%s-prov-generation";
    public static final String PROV_ACTIVITY_PROPERTY = "activity";
    public static final String PROV_ACTIVITY_INSTANCE = "conversion-workflow";
    public static final String ORIGINAL_COLLECTION_DESCRIPTION =
            """
            VoxEU.org – CEPR’s policy portal – was set up in June 2007 to promote "research-based policy
            analysis and commentary by leading economists". Vox's audience consists of economists working in the
            public sector, private sector, academia and media – as well as students of economics in the broad
            sense. Vox columns cover all fields of economics broadly defined and is widely read (the site
            receives about a half million page views per month).
            See https://voxeu.org/pages/about-vox for more.
            """;
}