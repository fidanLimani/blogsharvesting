package blog_scrape;

import blog_model.*;
import blog_model.VoxBlogPost;
import enums.VoxHarvestType;

import enums.VoxPostType;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rdf_conversion.BlogPostRDFConversion;
import utils.BlogUtils;
import utils.StringUtils;

import java.io.IOException;

import java.util.*;

import java.util.concurrent.TimeUnit;

import static constants.BlogHarvestingConstants.*;

// Blog scraping operations for Vox.eu
@Data
public class BlogScrapeVox implements BlogScrape {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlogScrapeVox.class);
    private BlogHarvestStatus blogHarvestStatus;
    private BlogUtils blogUtils;

    // IDation of blog post instances
    private UUID uuid;

    // Default constructor
    public BlogScrapeVox(){
        blogHarvestStatus = new BlogHarvestStatus();
        blogUtils = new BlogUtils();
    }

    /**
     * Retrieve the blog topics
     * @return blogTopics The topic label/text for each topic;
     **/
    public List<String> scrapeBlogTopics() {
        String voxURL = String.format(VOX_EU_URL_BASE, VoxHarvestType.Topic.getText());
        List<String> blogTopics = new LinkedList<>();

        try {
            Document doc = Jsoup.connect(voxURL).get();

            // Get the blog topics
            Elements topics = doc.select(TOPIC_SELECTOR);

            // System.out.printf("\nThere are %d VoxEU blog post topics:\n", topics.size());
            String elementText;
            for (Element element : topics) {
                elementText = element.text();
                // The topic "Frontiers of economic research" needs to drop the "of" in order to generate the reserved
                // URL assigned to it (frontiers-economic-research)
                if (elementText.equalsIgnoreCase("Frontiers of economic research")) {
                    elementText = elementText.replace("of ", "");
                }

                blogTopics.add(elementText);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return blogTopics;
    }

    /**
     * Scrape all blog post URLs on a topic and store the resulting BlogHarvesterStatus instance in a file.
     * @param blogTopic A blog topic or category
     * @return postsURLs Blog post URLs for that category
     */
    public Set<String> scrapeBlogPostURLsFromTopic(String blogTopic) {
        BlogHarvestStatus blogHarvestStatus;
        Set<String> postsURLs = new HashSet<>();

        String topicURL = String.format(VOX_EU_URL_TOPIC, StringUtils.generateTopicURL(blogTopic));
        try {
            blogHarvestStatus = new BlogHarvestStatus();

            // Update the BlogHarvesterStatus instance
            blogHarvestStatus.setTopic(blogTopic);
            blogHarvestStatus.setTopicURL(topicURL);

            Document doc = Jsoup.connect(topicURL).get();

            // How many post pages are there?
            Element paginationInfoElement = doc.select("li.pager-current").first();
            if (paginationInfoElement != null) {
                String paginationInfo = paginationInfoElement.text();
                int maxPageNumber = blogUtils.extractMaxPage(paginationInfo);
                // System.out.println("Max page number: " + maxPageNumber);
                for (int i = 0; i < maxPageNumber; i++) {
                    // System.out.println(topicURL + String.format(BLOG_PAGE, i));
                    LOGGER.info("Harvesting " + i + " / " + maxPageNumber + " pages");

                    doc = Jsoup.connect(topicURL + String.format(BLOG_PAGE, i)).get();
                    Elements pageItems =
                            doc.select("div.region.region-content-bottom div.content div.view-content div.item-list ul span.field-content > a");

                    for (Element e : pageItems) {
                        // System.out.println(String.format(VOX_BASE_URL, e.attr("href")));
                        postsURLs.add(String.format(VOX_BASE_URL, e.attr("href")));
                    }

                    // Introduce a pause in-between harvesting the different (results) pages
                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException ie) {
                        Thread.currentThread().interrupt();
                    }
                }
            }

            // Get the top-5 posts of the category, which are separately featured from the rest
            // System.out.println("\nHarvesting the top 5 posts from this category...");
            Elements top5PostURLs = doc.select("h2.field-content > a[href]");
            for (Element e : top5PostURLs) {
                // System.out.printf((VOX_BASE_URL) + "%n", e.attr("href"));  // e.text()
                postsURLs.add(String.format(VOX_BASE_URL, e.attr("href")));
            }

            // Add the remaining two attributes to the BlogHarvesterStatus instance
            blogHarvestStatus.setPostURLs(postsURLs);
            blogHarvestStatus.setBlogPostCount(postsURLs.size());

            // Write the BlogHarvestStatus instance to the file
            blogUtils.storeBlogHarvestToJSON(blogHarvestStatus);

            // Confirm the harvest for a topic
            LOGGER.info("# of blog posts for topic <" + blogTopic + "> is: " + postsURLs.size());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return postsURLs;
    }

    /**
     * Scrape a blog post
     * @param blogPostURL Blog post URL
     * @return A BlogPostResource instance with the blog post information/metadata
     */
    public VoxBlogPost scrapePost(String blogPostURL) {
        LOGGER.info("Scraping blog post @ " + blogPostURL);
        VoxBlogPost blogPost = new VoxBlogPost();

        Element titleElement;
        Elements postAuthors;
        Element publicationDate;
        Element summary;
        Element blogPostContent;
        List<String> authorList;

        // Assign a unique IDer to the blog post instance
        uuid = UUID.randomUUID();
        blogPost.setUuid(uuid);

        // Add blog post URL
        blogPost.setURL(blogPostURL);

        try {
            Document doc = Jsoup.connect(blogPostURL).get();

            // Initialize the structures that constitute a BlogPost instance
            authorList = new LinkedList<>();

            // Is it text-based or a (n audio) podcast?
            Element postType = doc.select(VOX_POST_PODCAST).first();
            if (postType != null) {
                // System.out.println("Harvesting a podcast.");
                blogPost.setBlogPostType(VoxPostType.Podcast);

                // There are few differences in handling the common metadata elements b/w a "podcast" and text post
                // Title: Check for NPException
                titleElement = doc.select(VOX_POST_PODCAST_TITLE).first();
                if (titleElement != null) {
                    blogPost.setTitle(titleElement.text());
                }
            } else {
                // System.out.println("Harvesting a text-based post.");
                blogPost.setBlogPostType(VoxPostType.Text);

                // Title
                titleElement = doc.select(VOX_POST_TITLE).first();

                // Some blog posts from VoxEU.org are structured a bit differently
                if (titleElement == null) {
                    titleElement = doc.select(VOX_POST_TITLE_V02).first();
                }

                // Some elements, including the title, could be empty
                if(titleElement != null) {
                    // System.out.println("Title: " + titleElement.text());
                    blogPost.setTitle(titleElement.text());
                }

                // Article content
                blogPostContent = doc.select(VOX_ARTICLE_CONTENT).first();
                if (blogPostContent != null) {
                    // Check for the "Endnotes" element, if present (<h2>Endnotes</h2>), and its items
                    // String endNotesElementString = "h2:contains(Endnotes)";
                    Elements endNoteElements = blogPostContent.select(VOX_ARTICLE_ENDNOTE_ELEMENT);
                    Elements endNoteItems = blogPostContent.select(VOX_ARTICLE_ENDNOTE_ITEMS);

                    // The main textual body of the post up to (and including) the <h2>References</h2> element
                    Elements finalBlogPostContent = blogPostContent.select(VOX_ARTICLE_REFERENCE_ITEMS);

                    // Elements to remove from the blog post body
                    // Are there still tables in the content after removal? ToDo
                    Elements tableElements = blogPostContent.select(VOX_ARTICLE_TABLE_ELEMENT);
                    if(!tableElements.isEmpty()) {
                        finalBlogPostContent.removeAll(tableElements);
                    }

                    // Remove the "References" element (<h2>References</>)
                    Elements referencesElement = blogPostContent.select(VOX_ARTICLE_REFERENCE_ELEMENT);
                    finalBlogPostContent.remove(referencesElement.first());

                    // Add Endnotes to the blog post text
                    finalBlogPostContent.addAll(endNoteElements);
                    finalBlogPostContent.addAll(endNoteItems);

                    // Add the text values for each element of the blog post (in HTML)
                    StringBuilder blogPostText = new StringBuilder();
                    for (Element e : finalBlogPostContent) {
                        // As opposed to Jsoup.parse(finalBlogPostContent.html()).text();
                        blogPostText.append(e.text()).append(System.getProperty("line.separator"));
                    }

                    // Content
                    blogPost.setBody(blogPostText.toString());

                    // References
                    // Retrieve the blog post references; as before, remove the "<>References</>" element.
                    Elements blogPostReferences = blogPostContent.select(VOX_ARTICLE_CONTENT_ITEMS);
                    blogPostReferences.removeAll(finalBlogPostContent);
                    blogPostReferences.remove(referencesElement.first());

                    // Add the text values for each reference element (in HTML)
                    List<String> blogPostReferencesList = new LinkedList<>();
                    for(Element e : blogPostReferences) {
                        blogPostReferencesList.add(e.text());
                    }

                    // System.out.println("# of references: " + blogPostReferencesList.size());
                    blogPost.setReferences(blogPostReferencesList);
                }
            }

            // Authors and Publication date
            postAuthors = doc.select(VOX_POST_AUTHORS);
            String tempAuthor;
            // Are there authors for the blog post?
            if (!postAuthors.isEmpty()) {
                for (Element author : postAuthors) {
                    // System.out.println("Author: " + author.text());
                    tempAuthor = StringUtils.handleAuthorName(author.text())
                            .replace("\"", "")
                            .replace(".", "");
                    // System.out.println(tempAuthor);
                    authorList.add(tempAuthor);
                }

                blogPost.setAuthors(authorList);
            }

            // Publication date
            publicationDate = doc.select(VOX_PUBLICATION_DATE).first();
            if (publicationDate != null) {
                // System.out.println("Date: " + publicationDate.text());
                blogPost.setPublicationDate(StringUtils.handleDateFormat(publicationDate.text()));
            }

            // Article brief or abstract (the blog post terms it as a "teaser" :)
            summary = doc.select(VOX_ARTICLE_SUMMARY).first();
            if (summary != null) {
                // System.out.println("Article summary: " + summary.text());
                blogPost.setSummary(summary.text());
            }

            // Topics
            Elements blogPostTopicElements = doc.select(VOX_ARTICLE_TOPICS);
            List<AbstractTopicCategory> categoryList = blogUtils.retrieveTopics(blogPostTopicElements);

            List<VoxTopic> blogPostTopics = new LinkedList<>();
            List<VoxTag> blogPostTags = new LinkedList<>();

            for (AbstractTopicCategory category : categoryList) {
                if(category instanceof VoxTopic) {
                    blogPostTopics.add((VoxTopic) category);
                }

                if (category instanceof VoxTag) {
                    blogPostTags.add((VoxTag) category);
                }
            }

            // Add topical categories to the BlogPost instance
            blogPost.setTopics(blogPostTopics);
            blogPost.setTags(blogPostTags);

            // STW terms: Assign them to a given (title, body) of the blog post
            List<STWTerm> stwTerms;
            stwTerms = blogUtils.assignStwTerms(blogPost.getTitle() + blogPost.getBody(), TERM_ASSIGNMENT_COUNT);
            blogPost.setStwTerms(stwTerms);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return blogPost;
    }

    // Test the app
    public static void main(String[] args) {
        BlogScrapeVox blogScrapeVox = new BlogScrapeVox();

        // blogScrapeVox.scrapePost("https://voxeu.org/article/revealing-implicit-stereotypes");

        // 1. Scrape the topics and the number of posts for each topic
        blogScrapeVox.scrapeBlogTopics();

        // 2. Scrape all the posts from a certain topic.
        String blogTopic = "Productivity and Innovation";
        Set<String> blogPostList = blogScrapeVox.scrapeBlogPostURLsFromTopic(blogTopic);
        System.out.printf("There are %d blog posts in this categry\n", blogPostList.size());
    }
}