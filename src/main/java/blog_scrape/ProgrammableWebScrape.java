package blog_scrape;

import enums.VoxHarvestType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static constants.BlogHarvestingConstants.*;

public class ProgrammableWebScrape {
    List<String> urlAvoidList = List.of("https://www.programmableweb.com/api/directory-open-access-journals");

    /**
     * Scrape only the topics and the posts per topic. An example: Financial markets -> 1064
     * Cosnider returning the topic label/text and the number of posts for each topic;
     **/
    public void scrapeAPIList() {
        String targetURL = "https://www.programmableweb.com/category/science/apis?category=20070&page=%d";
        int i = 27;

        /*
        while(i <= 28) {
            System.out.println(String.format(targetURL, i));

            i++;
        }
         */

        try {
            Document doc = Jsoup.connect(String.format(targetURL, i)).get();
            System.out.printf("URL: %s\n\n", String.format(targetURL, i));

            // Get the API list
            // String tableSelector = "article.clearfix.apis-directory table.views-table.cols-5.table > tbody > tr.odd.views-row-first > td.views-field.views-field-pw-version-title";
            String tableSelector = "div.view-content > table > tbody > tr.odd.views-row-first";
            Element oddFirstTableRow = doc.selectFirst(tableSelector);
            Elements evenTableRow = doc.select("div.view-content > table > tbody > tr.even");
            Elements oddTableRow = doc.select("div.view-content > table > tbody > tr.odd");
            // System.out.println("The very first element in the table: \n" + oddFirstTableRow);

            // System.out.println("The first element of the table:");
            // String firstAPI = getAPINameAndYear(oddFirstTableRow);
            // getAPIYear(firstAPI);

            // Combine all the Elements in an array
            List<Element> apiElements = new LinkedList<>();
            apiElements.add(oddFirstTableRow);
            apiElements.addAll(evenTableRow); // vs. 0) evenTableRow.forEach(elmnt -> apiElements.add(elmnt)); vs. 1) evenTableRow.forEach(apiElements::add);
            apiElements.addAll(oddTableRow); // vs. 0) oddTableRow.forEach(elmnt -> apiElements.add(elmnt)); vs. 1) oddTableRow.forEach(apiElements::add);

            System.out.println("Total API services: " + apiElements.size());

            // System.out.println("The other APIs in the table...");
            // System.out.println("# of even table row elements: " + evenTableRow.size());
            for (Element element : apiElements) {
                String apiNameAndYear = getAPINameAndYear(element);
                getAPIYear(apiNameAndYear);

                // Introduce a pause in-between accessing different Web pages.
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve the API name and year submitted to ProgrammableWeb
     */
    private String getAPINameAndYear(Element inputElement) {
        final String baseURI = "https://www.programmableweb.com%s";
        String apiElementStr = "td.views-field.views-field-pw-version-title > a";
        Element apiElement = inputElement.select(apiElementStr).first();
        String apiHref = "";

        if (apiElement != null) {
            apiHref = apiElement.attr("href");
            String apiName = apiElement.text();
            // System.out.printf("API name: %s,\t API URL: %s\n", apiName, String.format(baseURI, apiHref));
        }

        return String.format(baseURI, apiHref);
    }

    /**
     * Retrieve the API submission year
     */
    private void getAPIYear(String apiURL) {
        // Check if the URL is that of a service, or does it point to another directory
        if (!urlAvoidList.contains(apiURL)) {
            String apiElementStr = "div.version.pull-left.col-lg-2";
            try {
                Document apiDoc = Jsoup.connect(apiURL).get();
                String apiYear = apiDoc.select(apiElementStr).get(2).text();
                System.out.printf("%s\t %s\n", apiURL, apiYear.replace("Submitted:", ""));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Test the scraping
    public static void main(String[] args) {
        ProgrammableWebScrape webScrape = new ProgrammableWebScrape();
        webScrape.scrapeAPIList();

        /*
        String testURL = "https://www.programmableweb.com/api/usgs-search";
        webScrape.getAPIYear(testURL);
         */
    }
}
