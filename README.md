### The blog adapter

This is one of the "data adapters" for the Scholarly Artifacts Knowledge Graph (KG) project. 
It harvests, parses, enriches, converts to RDF and stores blog posts to the KG.

`BlogHarvester` is the interface: as each blog post collection provides certain access mechanism 
(HTML, API, etc.), this is the interface one extends to consider (and implement) the 
specific details of adding a blog post collection resources to the KG. 
